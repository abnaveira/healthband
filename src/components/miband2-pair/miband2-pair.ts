import {Component, Input} from '@angular/core';
import {Band, ConnectionStatus} from "../../providers/band-manager/band";

/**
 * Generated class for the Miband2PairComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'miband2-pair',
  templateUrl: 'miband2-pair.html'
})
export class Miband2PairComponent {

  @Input('device') device:Band;

  constructor() {

  }

  retryConnection() {
    if(this.device.connected==ConnectionStatus.DISCONNECTED){
      this.device.connect(true);
    }
  }
}
