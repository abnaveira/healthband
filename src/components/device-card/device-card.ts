import {Component, Input} from '@angular/core';
import {Band, ConnectionStatus} from "../../providers/band-manager/band";
import { SupportedBands} from "../../providers/band-manager/band-factory";

/**
 * Generated class for the DeviceCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'device-card',
  templateUrl: 'device-card.html'
})
export class DeviceCardComponent {

  @Input('device') device:Band;

  constructor() {

  }

  getMiBandName():string{return SupportedBands.MI_BAND_2;}



}
