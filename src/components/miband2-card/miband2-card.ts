import {Component, Input} from '@angular/core';
import {Band, ConnectionStatus} from "../../providers/band-manager/band";
import {BandManagerProvider} from "../../providers/band-manager/band-manager";
import {TranslateService} from "@ngx-translate/core";
import {AlertController, ModalController, ToastController} from "ionic-angular";
import {HeartRateSnapshotPage} from "../../pages/heart-rate-snapshot/heart-rate-snapshot";
import {MiBand2} from "../../providers/band-manager/mi-band-2";

/**
 * Generated class for the Miband2CardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'miband2-card',
  templateUrl: 'miband2-card.html'
})
export class Miband2CardComponent {

  @Input('device') device:Band;


  constructor(public bandManager:BandManagerProvider,public translateService: TranslateService) {

  }

  changeSelectedBand(band:Band){
    this.bandManager.changeSelectedBand(band);
  }

  deleteDevice(band:Band){
    this.bandManager.deleteDevice(band);
  }

  connectDisconnect(band:Band){
    this.bandManager.connectDisconnect(band);
  }

  heartRateSnapshot(band:Band){
    this.bandManager.heartRateSnapshot(band);
  }

  findDevice(band:Band){
    this.bandManager.findDevice(band);
  }
}
