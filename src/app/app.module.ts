import { BrowserModule } from '@angular/platform-browser';
import {ErrorHandler, Injector, NgModule} from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {BLE} from "@ionic-native/ble";
import {HttpClient, HttpClientModule} from "@angular/common/http";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {MainPage} from "../pages/main/main";
import {MyDevicesPage} from "../pages/my-devices/my-devices";
import {DiscoverDevicesPage} from "../pages/discover-devices/discover-devices";
import { BandManagerProvider } from '../providers/band-manager/band-manager';
import {IonicStorageModule} from "@ionic/storage";
import {HeartRateSnapshotPage} from "../pages/heart-rate-snapshot/heart-rate-snapshot";
import { DataBaseProvider } from '../providers/data-base/data-base';
import {SQLite} from "@ionic-native/sqlite";
import {NativePageTransitions} from "@ionic-native/native-page-transitions";
import {BackgroundMode} from "@ionic-native/background-mode";
import {LoginPage} from "../pages/login/login";
import { WebManagerProvider } from '../providers/web-manager/web-manager';
import {WelcomePage} from "../pages/welcome/welcome";
import {RegisterPage} from "../pages/register/register";
import {UserProfilePopoverPage} from "../pages/user-profile-popover/user-profile-popover";
import {ProfileSettingsPage} from "../pages/profile-settings/profile-settings";
import {AndroidPermissions} from "@ionic-native/android-permissions";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {TimelineComponent, TimelineItemComponent, TimelineTimeComponent} from "../components/timeline/timeline";
import {StatsPage} from "../pages/stats/stats";
import {ParallaxHeaderDirective} from "../directives/parallax-header/parallax-header";
import {ProgressBarComponent} from "../components/progress-bar/progress-bar";
import {ConfigurationPage} from "../pages/configuration/configuration";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {AboutPage} from "../pages/about/about";
import { NotificationProvider } from '../providers/notification/notification';
import {DeviceCardComponent} from "../components/device-card/device-card";
import {Miband2CardComponent} from "../components/miband2-card/miband2-card";
import {PairInstructionsPopoverPage} from "../pages/pair-instructions-popover/pair-instructions-popover";
import {Miband2PairComponent} from "../components/miband2-pair/miband2-pair";
import { ConfigurationProvider } from '../providers/configuration/configuration';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MainPage,
    MyDevicesPage,
    DiscoverDevicesPage,
    StatsPage,
    HeartRateSnapshotPage,
    LoginPage,
    WelcomePage,
    RegisterPage,
    UserProfilePopoverPage,
    PairInstructionsPopoverPage,
    ProfileSettingsPage,
    TimelineComponent,
    TimelineItemComponent,
    TimelineTimeComponent,
    ParallaxHeaderDirective,
    ProgressBarComponent,
    ConfigurationPage,
    AboutPage,
    DeviceCardComponent,
    Miband2CardComponent,
    Miband2PairComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MainPage,
    MyDevicesPage,
    DiscoverDevicesPage,
    StatsPage,
    HeartRateSnapshotPage,
    LoginPage,
    WelcomePage,
    RegisterPage,
    UserProfilePopoverPage,
    PairInstructionsPopoverPage,
    ProfileSettingsPage,
    ConfigurationPage,
    AboutPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BLE,
    SQLite,
    AndroidPermissions,
    LocationAccuracy,
    LocalNotifications,
    NativePageTransitions,
    BackgroundMode,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BandManagerProvider,
    DataBaseProvider,
    WebManagerProvider,
    NotificationProvider,
    ConfigurationProvider
  ]
})
export class AppModule {
  static injector: Injector;

  constructor(injector: Injector) {
    AppModule.injector = injector;
  }
}



