import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {MainPage} from "../pages/main/main";
import {Storage} from "@ionic/storage";
import {WelcomePage} from "../pages/welcome/welcome";
import {AndroidPermissions} from "@ionic-native/android-permissions";
import {WebManagerProvider} from "../providers/web-manager/web-manager";
import {ProfileSettingsPage} from "../pages/profile-settings/profile-settings";
import {ConfigurationPage} from "../pages/configuration/configuration";
import {TranslateService} from "@ngx-translate/core";
import {AboutPage} from "../pages/about/about";
import {DiscoverDevicesPage} from "../pages/discover-devices/discover-devices";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('NAV') nav: Nav;

  rootPage:any;
  public pages: Array<{titulo: string, component:any, icon:string}>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
              storage:Storage,androidPermissions: AndroidPermissions, public webManager:WebManagerProvider,private translateService: TranslateService) {

    this.pages=[
      {titulo:'DISCOVER_DEVICES', component:DiscoverDevicesPage,icon:'bluetooth'},
      {titulo:'PROFILE_SETTINGS', component:ProfileSettingsPage,icon:'person'},
      {titulo:'CONFIGURATION',component:ConfigurationPage,icon:'settings'},
      {titulo:'ABOUT',component:AboutPage,icon:'information'}
    ];

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      //Language
      this.translateService.setDefaultLang('en');
      this.translateService.use(navigator.language.substring(0,2));

      statusBar.styleDefault();
      splashScreen.hide();

      storage.get(WebManagerProvider.AUTH_TOKEN_KEY).then(
        (key)=>{
          if(key==null){
            console.log("user not authenticated");
            this.rootPage=WelcomePage;

          }else {
            webManager.token=key;
            webManager.decodeTokenInfo();
            this.rootPage=MainPage;
          }
        }
      );
      androidPermissions.requestPermissions(
        [
          androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION,
          androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,
          androidPermissions.PERMISSION.BLUETOOTH,
          androidPermissions.PERMISSION.BLUETOOTH_ADMIN
        ]
      );
    });
  }

  goToPage(page){
    this.nav.push(page);
  }
}

