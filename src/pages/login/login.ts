import { Component } from '@angular/core';
import {
  App,
  IonicPage,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {MainPage} from "../main/main";
import {WebManagerProvider} from "../../providers/web-manager/web-manager";
import {TranslateService} from "@ngx-translate/core";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  todo:any={};
  private serverUp:boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage:Storage, public webManager:WebManagerProvider,
              private app:App,private toastCtrl: ToastController,private translateService: TranslateService) {
    this.serverUp=false;
  }

  logForm() {
        this.webManager.logIn(this.todo).then(
          ()=>{
            this.app.getRootNav().setRoot(MainPage);
          },
          (error)=>{
            let errorMessage:string='';
            switch (error.status){
              case 0: {
                this.translateService.get("TOAST_SERVER_CONNECTION").subscribe(
                  (value)=>{
                    errorMessage=value;
                  },(e)=>{console.log(e);}
                );
                break;
              }
              default :{
                errorMessage=error.error;
                break;
              }
            }
            this.toastCtrl.create({
              message: errorMessage,
              duration: 3000,
              position: 'bottom',
              cssClass:"toastStyle"
            }).present();
          }
        );
  }

}
