import {Component, Renderer2, ViewChildren, QueryList} from '@angular/core';
import {Events, NavController} from 'ionic-angular';
import { Chart } from 'chart.js';
import {DataBaseProvider} from "../../providers/data-base/data-base";
import {WebManagerProvider} from "../../providers/web-manager/web-manager";
import {BandManagerProvider} from "../../providers/band-manager/band-manager";
import {Storage} from "@ionic/storage";
/*
import {PreferencesUtil} from "../../model/PreferencesUtil";
*/
import {MainPage} from "../main/main";
import {NotificationProvider} from "../../providers/notification/notification";
import {ConfigurationProvider} from "../../providers/configuration/configuration";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChildren('cc') cardContent: QueryList<any>;
  @ViewChildren('ic') iconCloseable: QueryList<any>;

  showStepsBar:boolean=true;
  stepsProgress: number=1;
  constructor(public navCtrl: NavController, public db: DataBaseProvider,public renderer:Renderer2,
              public webManager:WebManagerProvider,public bandManager:BandManagerProvider,public storage:Storage,public events: Events,public notificationProvider:NotificationProvider,public configurationProvider:ConfigurationProvider) {

    this.events.subscribe(ConfigurationProvider.PREFERENCES_CHANGED_EVENT,()=>{
      if(this.configurationProvider.loaded) {
        this.getStepsValueNormalized();
      }
    });

    this.events.subscribe(ConfigurationProvider.PREFERENCES_LOADED_EVENT,()=>{
      if(this.configurationProvider.loaded) {
        this.getStepsValueNormalized();
      }
    });

  }

  ionViewDidLoad(){
    this.notificationProvider.setFirstNotification();
  }

  ionViewDidEnter(){
    if(this.configurationProvider.loaded){
      this.getStepsValueNormalized();
    }
  }

  getStepsValueNormalized(){

    let steps = this.bandManager.getDefaultBandSteps();
    this.showStepsBar=this.configurationProvider.confValues.showStepsObjectiveBar;
    if(+steps>=this.configurationProvider.confValues.stepsObjective){
      this.stepsProgress= 100;
    }else if(+steps<=0){
      this.stepsProgress= 1;
    }else{
      this.stepsProgress= (100/(this.configurationProvider.confValues.stepsObjective))*(+steps-this.configurationProvider.confValues.stepsObjective)+100
    }

  }


  toggleTimeline(item) {
    if(item.expanded){
      this.renderer.setStyle(this.cardContent.toArray()[this.notificationProvider.items.length-1-item.id].nativeElement,"max-height","0px");
      this.renderer.setStyle(this.iconCloseable.toArray()[this.notificationProvider.items.length-1-item.id].nativeElement,"opacity","1");

    }else{
      this.renderer.setStyle(this.cardContent.toArray()[this.notificationProvider.items.length-1-item.id].nativeElement,"max-height","500px");
      this.renderer.setStyle(this.iconCloseable.toArray()[this.notificationProvider.items.length-1-item.id].nativeElement,"opacity","0");

    }
    item.expanded=!item.expanded;
  }

  clearButton(){
    this.notificationProvider.setFirstNotification();
    /*Es necesario recargar la página por un bug con el efecto parallax que se produce al eliminar las notificaciones*/
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }


}
