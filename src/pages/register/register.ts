import { Component } from '@angular/core';
import {
  App,
  IonicPage,
  NavController,
  NavParams,
  Refresher,
  Toast,
  ToastController
} from 'ionic-angular';
import {WebManagerProvider} from "../../providers/web-manager/web-manager";
import {MainPage} from "../main/main";
import {TranslateService} from "@ngx-translate/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  private todo:any={lifeStyle:{},gender:{}};
  private lifeStyle:any;
  private gender:any;
  private serverUp:boolean;
  private toast:Toast;
  registerForm:FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public webManager:WebManagerProvider,public app:App,
              private toastCtrl: ToastController,private translateService: TranslateService,public formBuilder: FormBuilder) {
    this.serverUp=false;
    this.registerForm=this.formBuilder.group({
      name:['',Validators.compose([Validators.maxLength(30),Validators.minLength(1),Validators.required])],
      username:['',Validators.compose([Validators.maxLength(30),Validators.minLength(1),Validators.required])],
      password:['',Validators.compose([Validators.maxLength(30),Validators.minLength(4),Validators.required])],
      age:['',Validators.compose([Validators.max(150), Validators.pattern('[0-9]*'), Validators.required])],
      weight:['',Validators.compose([Validators.max(300), Validators.pattern('[0-9]*'), Validators.required])],
      height:['',Validators.compose([Validators.max(250), Validators.pattern('[0-9]*'), Validators.required])],
      gender:['',Validators.required],
      lifeStyle:['',Validators.required]});
  }
  ionViewDidEnter(){
    this.checkServer().then(
      ()=>{},
      ()=>{}
    )
  }
  checkServer(){
    return new Promise((resolve,reject)=> {
      this.serverUp = true;
      this.webManager.getUserOptions().then(
        (value: any) => {
          this.lifeStyle = value.lifeStyle;
          this.gender=value.gender;
          resolve(value);
        },
        (error) => {
          console.log(error);
          this.serverUp = false;
          this.handleError(error);
          reject();
        }
      );
    });
  }

  submitForm() {
    for(let ls of this.lifeStyle){
      if(ls.id==this.todo.lifeStyle.id){
        this.todo.lifeStyle.name=ls.name;
        this.todo.lifeStyle.text=ls.text;
        break;
      }
    }
    for(let g of this.gender){
      if(g.id==this.todo.gender.id){
        this.todo.gender.name = g.name;
        this.todo.gender.text= g.text;
        break;
      }
    }
    let registerData ={
      "name": this.todo.name,
      "username": this.todo.username,
      "password": this.todo.password,
      "healthStats": {
        "age":this.todo.age,
        "height":this.todo.height,
        "weight":this.todo.weight,
        "gender":this.todo.gender,
        "lifeStyle":this.todo.lifeStyle
      }
    };
    console.log(registerData);
    this.webManager.register(registerData).then(
      ()=>{
        this.app.getRootNav().setRoot(MainPage);
      },
      (error)=>{
        this.serverUp=false;
        this.handleError(error);
      }
    );
  }
  private handleError(e){
    this.translateService.get(["TOAST_SERVER_CONNECTION_PULL","TOAST_AN_ERROR_OCCURRED"]).subscribe(
      (values)=>{
        switch (e.status){
          case 0:{
            if (this.toast == null) {
              this.toast = this.toastCtrl.create({
                message: values.TOAST_SERVER_CONNECTION_PULL,
                showCloseButton: true,
                position: 'bottom',
                cssClass:"toastStyle"
              });
              this.toast.onDidDismiss(() => {
                console.log('Dismissed toast');
                this.toast = null;
              });
              this.toast.present();
            }
            break;
          }
          case 409:{
            this.toastCtrl.create({
              message: e.error,
              duration: 3000,
              position: 'bottom',
              cssClass:"toastStyle"
            }).present();
            break;
          }
          default:{
            this.toastCtrl.create({
              message: values.TOAST_AN_ERROR_OCCURRED,
              duration: 3000,
              position: 'bottom',
              cssClass:"toastStyle"
            }).present();
            break;
          }
        }
      },(e)=>{console.log(e);}
    )

  }

  doRefresh(refresher: Refresher) {
    let time=setTimeout(()=>{
      if(refresher.enabled){
        refresher.complete();
      }
    },15000);
    this.checkServer().then(
      ()=>{
          clearTimeout(time);
          if (this.toast != null) {
            this.toast.dismiss();
          }
          this.translateService.get("CONNECTION_ESTABLISHED").subscribe(
            (value)=>{
              this.toastCtrl.create({
                message: value,
                duration: 3000,
                position: 'bottom',
                cssClass:"toastStyle"
              }).present();
            },(e)=>{console.log(e);}
          )

          refresher.complete();
      },
      ()=>{
        refresher.complete();
      }
    );

  }
}
