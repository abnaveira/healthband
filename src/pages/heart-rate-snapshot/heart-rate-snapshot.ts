import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import {Band} from "../../providers/band-manager/band";

/**
 * Generated class for the HeartRateSnapshotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-heart-rate-snapshot',
  templateUrl: 'heart-rate-snapshot.html',
})
export class HeartRateSnapshotPage {
  public device:Band;
  public done :boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController) {
    this.device=this.navParams.get('dev');
    this.done=false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HeartRateSnapshotPage');
  }

  public close(){
    this.done=true;
    this.viewCtrl.dismiss({});
    this.device.heartRateSnapshotMeasured=false;
    this.device.heartRateSnapshotValue=" ";
  }

}
