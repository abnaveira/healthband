import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HeartRateSnapshotPage } from './heart-rate-snapshot';

@NgModule({
  declarations: [
    //HeartRateSnapshotPage,
  ],
  imports: [
    IonicPageModule.forChild(HeartRateSnapshotPage),
  ],
})
export class HeartRateSnapshotPageModule {}
