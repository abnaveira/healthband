import {Component, NgZone, ViewChild} from '@angular/core';
import {
  App,
  Events,
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  Tab, Tabs,
  ToastController
} from 'ionic-angular';
import {HomePage} from "../home/home";
import {MyDevicesPage} from "../my-devices/my-devices";
import {BandManagerProvider} from "../../providers/band-manager/band-manager";
import {DataBaseProvider} from "../../providers/data-base/data-base";
import {UserProfilePopoverPage} from "../user-profile-popover/user-profile-popover";
import {WebManagerProvider} from "../../providers/web-manager/web-manager";
import {BLE} from "@ionic-native/ble";
import {WelcomePage} from "../welcome/welcome";
import {StatsPage} from "../stats/stats";
import {Storage} from "@ionic/storage";
import {TranslateService} from "@ngx-translate/core";
import {NotificationProvider} from "../../providers/notification/notification";
import {ConfigurationProvider} from "../../providers/configuration/configuration";

/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
  @ViewChild('myTabs') tabRef: Tabs;

  public static MAIN_PAGE_DID_ENTER_EVENT:string="MAIN_PAGE_DID_ENTER_EVENT";
  title:string;
  homeTab: any;
  myDevicesTab: any;
  statsTab: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public bandManager:BandManagerProvider,public db:DataBaseProvider,public popoverCtrl:PopoverController,public ble:BLE,
              public webManager:WebManagerProvider,public app:App,public events: Events,private toastCtrl: ToastController,
              public storage:Storage,private translateService: TranslateService,public notificationProvider:NotificationProvider,public configurationProvider:ConfigurationProvider) {
    this.homeTab=HomePage;
    this.myDevicesTab=MyDevicesPage;
    this.statsTab=StatsPage;
    this.webManager.getUserData().then(
      ()=>{},(e)=>{
        console.log(e);
         this.handleWebErrors(e);
      }
    );
  }

  dataFetchSwitch() {
    this.bandManager.dataFetch=!this.bandManager.dataFetch;
    if(this.bandManager.dataFetch){
      this.bandManager.startBackgroundDataFetch().then(
        ()=>{},
        (e)=>{
          this.bandManager.dataFetch=!this.bandManager.dataFetch;
        }
      )
    }else{
      this.bandManager.stopBackgroundDataFetch();
    }
  }

  ionViewDidLoad(){
    this.events.subscribe(ConfigurationProvider.PREFERENCES_LOADED_EVENT,()=>{
      if(this.configurationProvider.loaded) {
        if(this.configurationProvider.confValues.language==null){
          this.configurationProvider.confValues.language=this.translateService.currentLang;
          this.configurationProvider.saveConfiguration();
        }
        this.translateService.use(this.configurationProvider.confValues.language);
      }
    });


    this.ble.startStateNotifications().subscribe(
      (value)=>{
        if(value=="on"){
          setTimeout(()=>{this.bandManager.connectAllDevices()},5000);
        }else if(value=="off"){
          //this.bandManager.disconnectAllDevices();
        }
      }
    );
    this.db.openSQLDatabase().then(
      ()=>{console.log('Data base opened');},
      ()=>{console.log('Data base closed');}
    );
  }

  private handleWebErrors(e){
    console.log('Handle web errors.');
    console.log(e);
    switch (e.status){
      case 401:{
        this.translateService.get("TOAST_AN_ERROR_OCCURRED").subscribe(
          (value)=>{
            this.toastCtrl.create({
              message: value,
              duration: 3000,
              position: 'bottom',
              cssClass:"toastStyle"
            }).present();
          },(e)=>{console.log(e);}
        );
        this.app.getRootNav().setRoot(WelcomePage);
        break;
      }
      case 0:{
        this.translateService.get("TOAST_SERVER_CONNECTION").subscribe(
          (value)=>{
          this.toastCtrl.create({
            message: value,
            duration: 3000,
            position: 'bottom',
            cssClass:"toastStyle"
          }).present();
        },(e)=>{console.log(e);}
        );

        break;
      }
      default:{
        this.translateService.get("TOAST_AN_ERROR_OCCURRED").subscribe(
          (value)=>{
            this.toastCtrl.create({
              message: value,
              duration: 3000,
              position: 'bottom',
              cssClass:"toastStyle"
            }).present();
          },(e)=>{console.log(e);});
        this.app.getRootNav().setRoot(WelcomePage);
        break;
      }
    }
  }

  ionViewWillUnload(){
    this.ble.stopStateNotifications();
    this.bandManager.logOut();
    this.notificationProvider.items=[];
  }

  tabChange(tab: Tab) {
    this.title=tab.tabTitle;
  }

  openUserProfilePopover(event) {
    let popover = this.popoverCtrl.create(UserProfilePopoverPage);
    popover.present({ev:event});
  }
}
