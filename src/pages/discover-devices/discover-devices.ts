import {Component, ElementRef, NgZone, Renderer2, ViewChild} from '@angular/core';
import {Events, IonicPage, NavController, NavParams, PopoverController, ToastController} from 'ionic-angular';
import {BLE} from "@ionic-native/ble";
import {BandManagerProvider} from "../../providers/band-manager/band-manager";
import {WebManagerProvider} from "../../providers/web-manager/web-manager";
import {Storage} from "@ionic/storage";
import {TranslateService} from "@ngx-translate/core";
import {Band, ConnectionStatus} from "../../providers/band-manager/band";
import {BandFactory} from "../../providers/band-manager/band-factory";
import {PairInstructionsPopoverPage} from "../pair-instructions-popover/pair-instructions-popover";
import {ConfigurationProvider} from "../../providers/configuration/configuration";


/**
 * Generated class for the DiscoverDevicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-discover-devices',
  templateUrl: 'discover-devices.html',
})
export class DiscoverDevicesPage {
  @ViewChild('buttonDiv') buttonDiv:any;
  @ViewChild('scanButton',{read: ElementRef}) scanButton:ElementRef;

  public scanDevices:Band[];
  scanning: boolean;
  taskTimer:any=null;

  constructor(public navCtrl: NavController, public navParams: NavParams,public ble:BLE,public ngZone:NgZone,
              public toastCtrl:ToastController,public bandManager:BandManagerProvider,public renderer:Renderer2,public webManager:WebManagerProvider,public storage:Storage,
              private translateService: TranslateService,public popoverCtrl:PopoverController,public events: Events,public configurationProvider:ConfigurationProvider) {
    this.scanDevices=[];
    this.scanning=false;
  }

  ionViewDidLoad() {
  }

  startBleScan() {
    if(this.scanning===false){
      this.bandManager.requestLocation().then(
        ()=>{
          this.bandManager.requestBluetoothPermission().then(
            ()=>{
              this.renderer.setStyle(this.buttonDiv.nativeElement,"height","30%");
              this.renderer.setStyle(this.scanButton.nativeElement,"height","20vh");
              this.renderer.setStyle(this.scanButton.nativeElement,"width","20vh");
              this.renderer.setStyle(this.scanButton.nativeElement,"background","url(\"assets/imgs/oval.svg\")");
              this.renderer.setStyle(this.scanButton.nativeElement,"background-size","contain");

              if(this.configurationProvider.loaded){
                this.taskTimer = setTimeout(()=>{this.stopScanning()},this.configurationProvider.confValues.bluetoothScanTime*1000);
              }else{
                this.taskTimer = setTimeout(()=>{this.stopScanning()},30*1000);
              }

              this.ble.startScan([]).subscribe(
                (device:any)=>{
                  console.log('NEW DEVICE');

                  if(BandFactory.bandSupported(device.name)){
                    if(!this.alreadyScanned(device.id)){
                      if(!this.bandManager.containsDevice(device.id))
                        this.onDeviceDiscover(device);
                    }
                  }
                });

              this.scanning=!this.scanning;
            },()=>{
              this.showToast("TOAST_ENABLE_BLUETOOTH")
            }
          );
        },
        ()=>{
          this.showToast("TOAST_ENABLE_LOCATION")
        }
      )
    }else{
      this.stopScanning();
    }
  }

  private stopScanning(){
    if(this.scanDevices.length<=0){
      this.renderer.setStyle(this.buttonDiv.nativeElement,"height","100%");
      this.renderer.setStyle(this.scanButton.nativeElement,"height","35vh");
      this.renderer.setStyle(this.scanButton.nativeElement,"width","35vh");
    }
    this.renderer.setStyle(this.scanButton.nativeElement,"background","");
    this.ble.stopScan();
    if(this.taskTimer!=null){
      clearTimeout(this.taskTimer);
    }
    this.scanning=!this.scanning;
  }

  private onDeviceDiscover(device:any){
    console.log('Discovered ' + JSON.stringify(device, null, 2));
    //let band = new Band(device.id,device.name);
    let band = BandFactory.getBand(this.bandManager,device.id,device.name);
    band.rssi=device.rssi;
    if(band!=null){
      this.ngZone.run(()=>{
        this.scanDevices.push(band);
      })
    }
  }

  private showToast(message) {
    this.translateService.get(message).subscribe(
    (value)=>{
      this.toastCtrl.create({
        message: value,
        position: 'bottom',
        duration: 3000,
        cssClass:"toastStyle"
      }).present();
    },(e)=>{console.log(e);}
  )
  }


  connectBand(device:Band) {
    device.connect(true);
    this.events.subscribe(Band.BAND_CONNECTED_EVENT+device.id,()=>{
      let index = this.scanDevices.indexOf(device, 0);
      if (index > -1) {
        this.scanDevices.splice(index, 1);
      }
      this.translateService.get('NEW_BAND').subscribe(
        (value:any)=> {
          this.toastCtrl.create({
            message:value,
            duration: 2000,
            position: 'bottom',
            cssClass:"toastStyle"
          }).present();
        },(e)=>{console.log(e);});
    });

    this.openUserPairPopover(device);
  }

  ionViewWillLeave() {
    this.events.unsubscribe(Band.BAND_CONNECTED_EVENT);
  }

    ionViewDidEnter(){
    for(let b of this.scanDevices){
      if(b.connected==ConnectionStatus.CONNECTED){
        let i =this.scanDevices.indexOf(b);
        if (i > -1) {
          this.scanDevices.splice(i, 1);
        }
      }
    }
  }

  private alreadyScanned(deviceId:string){
    for(let band of this.scanDevices){
      if(band.id==deviceId){
        return true;
      }
    }
    return false;
  }

  openUserPairPopover(band:Band) {
    let popover = this.popoverCtrl.create(PairInstructionsPopoverPage,{band:band},{cssClass:'pair-ìnstructions-popover'});
    popover.present();
  }

}
