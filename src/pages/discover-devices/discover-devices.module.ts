import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiscoverDevicesPage } from './discover-devices';

@NgModule({
  declarations: [
   // DiscoverDevicesPage,
  ],
  imports: [
    IonicPageModule.forChild(DiscoverDevicesPage),
  ],
})
export class DiscoverDevicesPageModule {}
