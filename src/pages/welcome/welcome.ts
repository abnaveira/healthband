import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {LoginPage} from "../login/login";
import {RegisterPage} from "../register/register";
import {DisableSideMenu} from "../../customDecorators/disable-side-menu.decorator";
import {LocalNotifications} from "@ionic-native/local-notifications";

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@DisableSideMenu()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {
  loginTab:any;
  registerTab:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private localNotifications: LocalNotifications) {
    this.loginTab=LoginPage;
    this.registerTab=RegisterPage;
  }

  ionViewDidLoad() {
    this.localNotifications.clearAll().catch((e)=>{console.log(e);});
  }

}
