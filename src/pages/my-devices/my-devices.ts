import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  App,
} from 'ionic-angular';
import { BandManagerProvider} from "../../providers/band-manager/band-manager";
import {DataBaseProvider} from "../../providers/data-base/data-base";
import {DiscoverDevicesPage} from "../discover-devices/discover-devices";

/**
 * Generated class for the MyDevicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-devices',
  templateUrl: 'my-devices.html',
})
export class MyDevicesPage {
  constructor(public navCtrl: NavController, public navParams: NavParams,public bandManager:BandManagerProvider,
              public db:DataBaseProvider,public app:App) {
  }

  ionViewDidLoad() {
  }

  ionViewDidEnter(){
    this.bandManager.updateDefaultDataAllDevices();
  }

  searchButton(){
    let nav:NavController=this.app.getRootNav();
    nav.push(DiscoverDevicesPage);
  }

}
