import {Component} from '@angular/core';
import {App, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {WebManagerProvider} from "../../providers/web-manager/web-manager";
import {Storage} from "@ionic/storage";
import {TranslateService} from "@ngx-translate/core";
import { BandManagerProvider} from "../../providers/band-manager/band-manager";
import {Band} from "../../providers/band-manager/band";
import {BandFactory, SupportedBands} from "../../providers/band-manager/band-factory";
import {ConfigurationProvider} from "../../providers/configuration/configuration";
/**
 * Generated class for the ConfigurationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configuration',
  templateUrl: 'configuration.html',
})
export class ConfigurationPage {

  public debugBand:string='D0:AF:F7:3A:5A:C4';
  public endpoint:string=WebManagerProvider.BASIC_ULR;
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage,public webManager:WebManagerProvider, public app:App,private translateService: TranslateService,
              public bandManager:BandManagerProvider,public configurationProvider:ConfigurationProvider) {

  }

  ionViewDidLoad() {
    }

  ionViewWillLeave(){
  this.configurationProvider.saveConfiguration();
  if(this.webManager.isAdmin){
      WebManagerProvider.changeEndpoint(this.endpoint);
  }
  }


  manualConnect() {
    let band:Band=BandFactory.getBand(this.bandManager,this.debugBand,SupportedBands.MI_BAND_2);
    band.connect(true);
  }
}
