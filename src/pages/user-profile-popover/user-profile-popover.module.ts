import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserProfilePopoverPage } from './user-profile-popover';

@NgModule({
  declarations: [
    //UserProfilePopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(UserProfilePopoverPage),
  ],
})
export class UserProfilePopoverPageModule {}
