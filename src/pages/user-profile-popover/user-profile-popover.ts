import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {WebManagerProvider} from "../../providers/web-manager/web-manager";
import {WelcomePage} from "../welcome/welcome";
import {BandManagerProvider} from "../../providers/band-manager/band-manager";
import {TranslateService} from "@ngx-translate/core";
import {ConnectionStatus} from "../../providers/band-manager/band";

/**
 * Generated class for the UserProfilePopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-profile-popover',
  templateUrl: 'user-profile-popover.html',
})
export class UserProfilePopoverPage {
  private name:string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public webManager:WebManagerProvider,
              public app:App,public viewController:ViewController,public bandManager:BandManagerProvider,private toastCtrl: ToastController,private translateService: TranslateService) {
    this.name=this.webManager.tokenInfo.sub;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserProfilePopoverPage');
  }

  logOut() {
    this.webManager.logOut();
    //se cierra el popover
    this.viewController.dismiss();
    //Volvemos a la página de inicio
    this.app.getRootNav().setRoot(WelcomePage);
  }

  dataFetchSwitch() {
    if(this.bandManager.dataFetch){
      this.bandManager.startBackgroundDataFetch().then(
        ()=>{},
        (e)=>{
          this.showToast(e);
          this.bandManager.dataFetch=!this.bandManager.dataFetch;
        }
      )
    }else{
      this.bandManager.stopBackgroundDataFetch();
    }
  }

  findSelectedBand() {
    if(this.bandManager.selectedBand!=null){
      if(this.bandManager.selectedBand.connected==ConnectionStatus.CONNECTED){
        this.bandManager.requestBluetoothPermission().then(
          ()=>{
            //this.bandManager.addPetition(Tasks.SEND_INMEDIATE_ALERT,this.bandManager.selectedBand);
            this.bandManager.selectedBand.sendSimpleAlert();
          },()=>{
            this.showToast('NOT_BLUETOOTH');
          }
        );
      }else{
        this.showToast('NOT_SELECTED_BAND_NOT_CONNECTED');
      }
    }else{
      this.showToast('NOT_NO_SELECTED_BAND');
    }
  }
  private showToast(message){
    this.translateService.get(message).subscribe(
      (value)=>{
        this.toastCtrl.create({
          message: value,
          duration: 2000,
          position: 'bottom',
          cssClass:"toastStyle"
        }).present();
      },(e)=>{console.log(e);}
    )


  }


}
