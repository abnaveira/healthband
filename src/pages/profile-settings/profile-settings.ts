import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {WebManagerProvider} from "../../providers/web-manager/web-manager";
import {DisableSideMenu} from "../../customDecorators/disable-side-menu.decorator";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {WelcomePage} from "../welcome/welcome";

/**
 * Generated class for the ProfileSettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@DisableSideMenu()
@Component({
  selector: 'page-profile-settings',
  templateUrl: 'profile-settings.html',
})
export class ProfileSettingsPage {
  public userProfile:any;
  public saveChangesBool:boolean;
  public lifeStyle:any;
  public gender:any;
  profileForm:FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public webManager:WebManagerProvider,public formBuilder: FormBuilder,
              public toastCtrl: ToastController,public translateService: TranslateService,private app:App) {
    this.userProfile={healthStats:{lifeStyle:{},gender:{}}};
    this.saveChangesBool=false;
    this.profileForm=this.formBuilder.group({
      name:['',Validators.compose([Validators.maxLength(30),Validators.minLength(1),Validators.required])],
      age:['',Validators.compose([Validators.max(150), Validators.pattern('[0-9]*'), Validators.required])],
      weight:['',Validators.compose([Validators.max(300), Validators.pattern('[0-9]*'), Validators.required])],
      height:['',Validators.compose([Validators.max(250), Validators.pattern('[0-9]*'), Validators.required])],
      gender:['',Validators.required],
      lifeStyle:['',Validators.required]});
  }

  ionViewDidLoad() {
    this.webManager.getUserData().then(
      (value:any)=>{
        this.userProfile=value;
        console.log(value);
        this.webManager.getUserOptions().then(
          (value:any)=>{
            console.log(value);
            this.lifeStyle=value.lifeStyle;
            this.gender=value.gender;
            console.log(this.gender)
          },
          (error)=>{
            console.log(error);
            this.handleWebErrors(error);
          }
        );
      },
      (error)=>{
        console.log(error);
        this.handleWebErrors(error);
      }
    );

  }

  saveChanges() {
    this.saveChangesBool=false;
    for(let ls of this.lifeStyle){
      if(ls.id==this.userProfile.healthStats.lifeStyle.id){
        this.userProfile.healthStats.lifeStyle.name=ls.name;
        this.userProfile.healthStats.lifeStyle.text=ls.text;
        break;
      }
    }
    for(let g of this.gender){
      if(g.id==this.userProfile.healthStats.gender.id){
        this.userProfile.healthStats.gender.name = g.name;
        this.userProfile.healthStats.gender.text= g.text;
        break;
      }
      console.log(this.userProfile);
    }
    this.webManager.updateUserProfile(this.userProfile).then(
      ()=>{
        console.log("Update done.");
        this.translateService.get('USER_PROFILE_UPDATED').subscribe(
          (value:any)=> {
            this.toastCtrl.create({
              message:value,
              duration: 2000,
              position: 'bottom',
              cssClass:"toastStyle"
            }).present();
          },(e)=>{console.log(e);});
      },(error)=>{
        console.log(error);
        this.handleWebErrors(error);
      }
    );
  }

  formChanged() {
    this.saveChangesBool=true;
  }

  private handleWebErrors(e){
    console.log('Handle web errors.');
    console.log(e);
    switch (e.status){
      case 401:{
        this.translateService.get("TOAST_AN_ERROR_OCCURRED").subscribe(
          (value)=>{
            this.toastCtrl.create({
              message: value,
              duration: 3000,
              position: 'bottom',
              cssClass:"toastStyle"
            }).present();
          },(e)=>{console.log(e);}
        );
        this.app.getRootNav().setRoot(WelcomePage/*,{error:e.error}*/);
        break;
      }
      case 0:{
        this.translateService.get("TOAST_SERVER_CONNECTION").subscribe(
          (value)=>{
            this.toastCtrl.create({
              message: value,
              duration: 3000,
              position: 'bottom',
              cssClass:"toastStyle"
            }).present();
          },(e)=>{console.log(e);}
        );

        break;
      }
      default:{
        this.translateService.get("TOAST_AN_ERROR_OCCURRED").subscribe(
          (value)=>{
            this.toastCtrl.create({
              message: value,
              duration: 3000,
              position: 'bottom',
              cssClass:"toastStyle"
            }).present();
          },(e)=>{console.log(e);});
        this.app.getRootNav().setRoot(WelcomePage/*,{error:"A error has ocurred"}*/);
        break;
      }
    }
  }
}
