import {Component, QueryList, Renderer2, ViewChildren} from '@angular/core';
import {IonicPage, NavController, NavParams, Refresher} from 'ionic-angular';
import {DataBaseProvider} from "../../providers/data-base/data-base";
import { Chart } from 'chart.js';
import {WebManagerProvider} from "../../providers/web-manager/web-manager";
import {TranslateService} from "@ngx-translate/core";

/**
 * Generated class for the StatsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-stats',
  templateUrl: 'stats.html',
})
export class StatsPage {
  @ViewChildren('canvas') canvas:QueryList<any>;

  @ViewChildren('cc') cardContent: QueryList<any>;
  barCharts:any;
  accordionExpanded:Array<boolean>;
  titles:Array<String>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: DataBaseProvider,public renderer:Renderer2,public webManager:WebManagerProvider, private translateService: TranslateService) {
    this.accordionExpanded=Array(4).fill(true);
    this.titles=["STEPS_CHART","HR_CHART","CALORIES_CHART", "DISTANCE_CHART"];
  }

  ionViewDidLoad() {
    for(let i =0;i<4;i++){
      this.renderer.setStyle(this.cardContent.toArray()[i].nativeElement,"webkitTransition","max-height 500ms, padding 300ms");
    }
    this.updateChart();
  }

  updateChart() {
    this.db.openSQLDatabase().then(
      ()=>{
        let date = new Date(Date.now());
        this.db.getAllMeasurements('HourMeasurements',this.webManager.tokenInfo.sub).then(
          (data:any) => {
            let minDate:Date=new Date(date.getFullYear(),date.getMonth(),date.getUTCDate(),date.getHours(),0,0,0);
            let maxDate:Date=new Date(date.getFullYear(),date.getMonth(),date.getUTCDate(),date.getHours()+1,0,0,0);
            console.log(data.dates);
            const options =[{
              type: 'time',
              time: {
                min:minDate,
                max:maxDate,
                stepSize:10,
                unit:'minute',
                displayFormats: {
                  'minute':'H:mm'
                }
              },
            }];
            this.translateService.get(['STEPS_U','DISTANCE','CALORIES','HR','HOUR']).subscribe(
              (value:any)=> {
                this.showChart(0, data.dates, data.steps, minDate, maxDate, options, value.STEPS_U, value.HOUR, '#3498db');
                this.showChart(5, data.dates, data.heart, minDate, maxDate, options,value.HR+" (BPM)",value.HOUR, '#c0392b');
                this.showChart(10, data.dates, data.calories, minDate, maxDate, options,value.CALORIES+" (kCal)",value.HOUR, '#2ecc71');
                this.showChart(15, data.dates, data.distance, minDate, maxDate, options,value.DISTANCE+" (m)", value.HOUR, '#9b59b6');
              });
          },
          (error) => {
            console.log(error);
          }
        );
        this.db.getAllMeasurements('DayMeasurements',this.webManager.tokenInfo.sub).then(
          (data:any) => {
            let minDate:Date=new Date(date.getFullYear(),date.getMonth(),date.getUTCDate(),0,0,0,0);
            let maxDate:Date=new Date(date.getFullYear(),date.getMonth(),date.getUTCDate()+1,0,0,0,0);
            const options =[{
              type: 'time',
              time: {
                min:minDate,
                max:maxDate,
                stepSize:4,
                unit:'hour',
                displayFormats: {
                  'hour':'HH:mm'
                }
              }
            }];
            this.translateService.get(['STEPS_U','DISTANCE','CALORIES','HR','DAY']).subscribe(
              (value:any)=> {
                this.showChart(1, data.dates, data.steps, minDate, maxDate, options,value.STEPS_U, value.DAY, '#3498db');
                this.showChart(6, data.dates, data.heart, minDate, maxDate, options,value.HR +" (BPM)",value.DAY, '#c0392b');
                this.showChart(11, data.dates, data.calories, minDate, maxDate, options,value.CALORIES+" (kCal)", value.DAY, '#2ecc71');
                this.showChart(16, data.dates, data.distance, minDate, maxDate, options,value.DISTANCE+" (m)",value.DAY, '#9b59b6');
              });
          },
          (error) => {console.log(error);}
        );
        this.db.getAllMeasurements('WeekMeasurements',this.webManager.tokenInfo.sub).then(
          (data:any) => {
            let minDate:Date=new Date(date.getFullYear(),date.getMonth(),date.getUTCDate()+1,0,0,0,0);
            minDate.setDate(minDate.getUTCDate()-date.getDay());
            const l = data.dates.length-1;
            let maxDate:Date=new Date(date.getFullYear(),date.getMonth(),date.getUTCDate(),0,0,0,0);
            maxDate.setDate(maxDate.getUTCDate()+1+(6-date.getDay()));
            const options =[{
              type: 'time',
              time: {
                min:minDate,
                max:maxDate,
                stepSize:1,
                unit:'day',
                displayFormats: {
                  'day':'dd'
                }
              }
            }];
            this.translateService.get(['STEPS_U','DISTANCE','CALORIES','HR','WEEK']).subscribe(
              (value:any)=> {
                this.showChart(2, data.dates, data.steps, minDate, maxDate, options,value.STEPS_U,value.WEEK, '#3498db');
                this.showChart(7, data.dates, data.heart, minDate, maxDate, options,value.HR+" (BPM)", value.WEEK, '#c0392b');
                this.showChart(12, data.dates, data.calories, minDate, maxDate, options,value.CALORIES+" (kCal)", value.WEEK, '#2ecc71');
                this.showChart(17, data.dates, data.distance, minDate, maxDate, options,value.DISTANCE+" (m)", value.WEEK, '#9b59b6');
              });
          },
          (error) => {console.log(error);}
        );
        this.db.getAllMeasurements('MonthMeasurements',this.webManager.tokenInfo.sub).then(
          (data:any) => {
            let minDate:Date=new Date(date.getFullYear(),date.getMonth(),1,0,0,0,0);
            let maxDate:Date=new Date(date.getFullYear(),date.getMonth(),this.daysInMonth(date.getMonth(),date.getFullYear()),0,0,0,0);
            const options=[{
              type: 'time',
              time: {
                min:minDate,
                max:maxDate,
                stepSize:5,
                unit:'day',
                displayFormats: {
                  'day':'DD'
                }
              }
            }];
            this.translateService.get(['STEPS_U','DISTANCE','CALORIES','HR','MONTH']).subscribe(
              (value:any)=> {
                this.showChart(3, data.dates, data.steps, minDate, maxDate, options,value.STEPS_U, value.MONTH, '#3498db');
                this.showChart(8, data.dates, data.heart, minDate, maxDate, options,value.HR+" (BPM)" , value.MONTH, '#c0392b');
                this.showChart(13, data.dates, data.calories, minDate, maxDate, options,value.CALORIES+" (kCal)", value.MONTH, '#2ecc71');
                this.showChart(18, data.dates, data.distance, minDate, maxDate, options,value.DISTANCE+" (m)", value.MONTH, '#9b59b6');
              });
          },
          (error) => {console.log(error);}
        );
        this.db.getAllMeasurements('YearMeasurements',this.webManager.tokenInfo.sub).then(
          (data:any) => {
            let minDate:Date=new Date(date.getFullYear(),0,0,0,0,0,0);
            let maxDate:Date=new Date(date.getFullYear()+1,0,0,0,0,0,0);
            const options=[{
              type: 'time',
              time: {
                min:minDate,
                max:maxDate,
                stepSize:1,
                unit:'month',
                displayFormats: {
                  'month':'MMM'
                }
              }
            }];
            this.translateService.get(['STEPS_U','DISTANCE','CALORIES','HR','YEAR']).subscribe(
              (value:any)=> {
                this.showChart(4, data.dates, data.steps, minDate, maxDate, options,value.STEPS_U, value.YEAR, '#3498db');
                this.showChart(9, data.dates, data.heart, minDate, maxDate, options,value.HR +" (BPM)", value.YEAR, '#c0392b');
                this.showChart(14, data.dates, data.calories, minDate, maxDate, options,value.CALORIES+" (kCal)", value.YEAR, '#2ecc71');
                this.showChart(19, data.dates, data.distance, minDate, maxDate, options,value.DISTANCE+" (m)", value.YEAR, '#9b59b6');
              });
          },
          (error) => {console.log(error);}
        );
      },
      (error)=>{
        console.log(error);
      }
    );
  }


  private daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
  }

  public showChart(i,labels,data,minDate,maxDate,xAxesOptions,label,title,color){
    this.barCharts = (new Chart(this.canvas.toArray()[i].nativeElement, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [{
          label: label,
          data: data,
          borderWidth: 1,
          backgroundColor:color
        }]
      },
      options: {
        legend: {
          display: false
        },
        title: {
          display: true,
          text: title
        },
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }],
          xAxes: xAxesOptions
        }
      }
    }));
  }


  toggleAccordion(i) {
    if(this.accordionExpanded[i]){
      this.renderer.setStyle(this.cardContent.toArray()[i].nativeElement,"max-height","0px");
      this.renderer.setStyle(this.cardContent.toArray()[i].nativeElement,"padding","0px 16px");
    }else{
      this.renderer.setStyle(this.cardContent.toArray()[i].nativeElement,"max-height","300px");
      this.renderer.setStyle(this.cardContent.toArray()[i].nativeElement,"padding","13px 16px");
    }
    this.accordionExpanded[i] = !this.accordionExpanded[i];
  }

  doRefresh(refresher: Refresher) {
    this.db.updateDatabase(this.webManager.tokenInfo.sub).then(
      ()=>{
        this.updateChart();
        console.log("data added perfectly.");
        refresher.complete();
      },(e)=>{
        console.log(e);
        refresher.complete();
      }
    )
  }

}
