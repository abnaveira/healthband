import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PairInstructionsPopoverPage } from './pair-instructions-popover';

@NgModule({
  declarations: [
    PairInstructionsPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(PairInstructionsPopoverPage),
  ],
})
export class PairInstructionsPopoverPageModule {}
