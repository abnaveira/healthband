import { Component } from '@angular/core';
import {Events, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Band} from "../../providers/band-manager/band";
import {SupportedBands} from "../../providers/band-manager/band-factory";

/**
 * Generated class for the PairInstructionsPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pair-instructions-popover',
  templateUrl: 'pair-instructions-popover.html',
})
export class PairInstructionsPopoverPage {
  band:Band;
  constructor(public navCtrl: NavController, public navParams: NavParams,public events:Events,public viewController:ViewController) {
    this.band=navParams.get('band');
  }

  ionViewDidLoad() {
    this.events.subscribe(Band.BAND_CONNECTED_EVENT+this.band.id,()=>{
      this.viewController.dismiss();
    });
  }

  getMiBandName():string{return SupportedBands.MI_BAND_2;}


}
