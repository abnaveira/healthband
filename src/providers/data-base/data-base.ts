import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import {Platform, ToastController} from "ionic-angular";
import {Observable} from "rxjs/Observable";
import {WebManagerProvider} from "../web-manager/web-manager";
import {TranslateService} from "@ngx-translate/core";

/*
  Generated class for the DataBaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class DataBaseProvider {
  private db:SQLiteObject;
  private isOpen:boolean;
  constructor(public platform:Platform,public http: HttpClient,public SQLite:SQLite,public webManager:WebManagerProvider,public translateService: TranslateService,
              public toastCtrl: ToastController) {

  }

  public openSQLDatabase(){
    return new Promise((resolve,reject)=>{
      if(this.isOpen){
        resolve();
      }else{
        this.platform.ready().then(()=>{
          this.SQLite.create({
            name:'healthBandDatabase.db',
            location:'default'
          }).then((db:SQLiteObject)=>{
              this.db=db;
              this.isOpen=true;
              this.createAllTables().then(
                ()=>{
                  resolve();
                },
                (error)=>{
                  reject(error);
                }
              )
            },
            (error)=>{
              reject(error);
            });
        },(error)=>reject(error));
      }
    });
  }

  private createAllTables(){
    return new Promise((resolve,reject)=>{
      Promise.all([
        this.createTable('HourMeasurements'),
        this.createTable('DayMeasurements'),
        this.createTable('WeekMeasurements'),
        this.createTable('MonthMeasurements'),
        this.createTable('YearMeasurements')
      ]).then(
        ()=>{
          console.log('Tables created.');
          resolve();
        },(e)=>{
          console.log(e);
          reject();
        }
      );
    })

  }
  private createTable(name){
    //this.db.executeSql("DROP TABLE ["+name+"]",[]);
    return this.db.executeSql("CREATE TABLE IF NOT EXISTS "+name+" (id INTEGER PRIMARY KEY AUTOINCREMENT, username VARCHAR(30), heartRate INTEGER, steps INTEGER, calories INTEGER, distance REAL, date DATETIME2)",[]);
  }

  public updateDatabase(username){
    return new Promise((resolve,reject)=>{
      let promises=[];
      this.webManager.getMeasurements().then(
        (measurements:any)=>{
          console.log(measurements);
          promises.push(this.doUpdate('HourMeasurements',username,measurements[0]));
          promises.push(this.doUpdate('DayMeasurements',username,measurements[1]));
          promises.push(this.doUpdate('WeekMeasurements',username,measurements[2]));
          promises.push(this.doUpdate('MonthMeasurements',username,measurements[3]));
          promises.push(this.doUpdate('YearMeasurements',username,measurements[4]));
          Promise.all(promises).then(            ()=>{resolve()},()=>{reject();}
          )
        },(e)=>{
          reject(e);
          this.translateService.get("TOAST_SERVER_CONNECTION").subscribe(
            (value)=>{
              this.toastCtrl.create({
                message: value,
                duration: 1000,
                position: 'bottom'
              }).present();
            },(e)=>{console.log(e);}
          );
        }
      )
    })
  }


  private doUpdate(table,username,data){
    return new Promise((resolve,reject)=>{
      this.deleteWithUsername(table,username).then(
        ()=>{
          this.insertMeasurements(table,username,data).then(
            ()=>{resolve();},()=>{reject();}
          )
        },
        (e)=>{reject(e);}
      );
    })
  }
  private deleteWithUsername(table,username){
    return this.db.executeSql("DELETE FROM "+table+" WHERE username=?",[username]);
  }

  public insertMeasurements(table,username,data:any){
      let promises =[];
      for(let m of data) {
        let sql = "INSERT INTO "+table+" (username, heartRate, steps, calories, distance, date) VALUES (?, ?, ?, ?, ?, ?)";
        promises.push(this.db.executeSql(sql, [username, m.heartRate, m.steps, m.calories, m.distance, m.date]));
      }
      return Promise.all(promises);
  }

  public getAllMeasurements(table,username){
    return new Promise((resolve,reject)=>{
      this.db.executeSql("SELECT * FROM "+table+" WHERE username = ?",[username]).then(
        (data)=>{
          let steps=[];
          let heart=[];
          let distance=[];
          let calories=[];
          let labels=[];
          let dates=[];
          if(data.rows.length>0){
            for (let i=0;i<data.rows.length;i++){
              let date:Date = new Date(data.rows.item(i).date)

              labels.push(date.getHours()+':'+date.getMinutes());
              dates.push(date);
              heart.push(data.rows.item(i).heartRate);
              distance.push(data.rows.item(i).distance);
              calories.push(data.rows.item(i).calories);
              steps.push(data.rows.item(i).steps);
            }
          }
          data = {dates:dates,heart:heart,distance:distance,calories:calories,steps:steps};
          resolve(data);

        },(error)=>{
          reject(error);
        }
      )
    })
  }
}


