import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {WebManagerProvider} from "../web-manager/web-manager";
import {BackgroundMode} from "@ionic-native/background-mode";
import {TranslateService} from "@ngx-translate/core";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {BandManagerProvider, NotificationsID} from "../band-manager/band-manager";

/*
  Generated class for the NotificationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificationProvider {
  public items:{id:number,title:string,content:string,icon:string,time:any,expanded:boolean,alert:number}[]= [];
  month= ['Jan.','Feb.','Mar.','Apr.','May.','Jun.','Jul.','Aug.','Sep.','Oct.','Nov.','Dec.'];
  backgroundJob:any;
  constructor(public http: HttpClient,private webManager:WebManagerProvider,private backgroundMode: BackgroundMode,private translateService: TranslateService,private localNotifications: LocalNotifications) {

  }

  public setFirstNotification(){
    this.translateService.get(["DEFAULT_NOT_TITLE","DEFAULT_NOT_BODY"]).subscribe(
      (values)=>{
        let date = new Date(Date.now());
        this.items=[];
        this.items.push({id:0,title:values.DEFAULT_NOT_TITLE,content:values.DEFAULT_NOT_BODY,icon:"information-circle",time:{subtitle:date.getDate()+" "+this.month[date.getMonth()],title:(date.getHours()<10?'0':'') + date.getHours()+":"+(date.getMinutes()<10?'0':'') + date.getMinutes()},expanded:true,alert:0})
      },(e)=>{console.log(e);}
    )
  }

  public addNotifications(notifications:any,bandManager:BandManagerProvider){
    let i:number =1;
    if(this.items.length>1){
      i=this.items[this.items.length-1].id+1;
    }
    for(let not of notifications){
      const title:string=not.name+"_TITLE";
      const body:string = not.name+"_BODY";
      this.translateService.get([title,body]).subscribe(
        (trad:any)=>{

          let content:string=trad[body];
          const values:string[]=not.notificationValues.split(" ");
          let j: number;
          for(j=0;j<values.length;j++){
            let aux = j+1;
            content=content.replace("val"+aux,values[j]);
            console.log("val"+aux+"   "+values[j]);
          }
          this.localNotifications.schedule(
            {id:NotificationsID.NOTIFICATION,text:trad[title]}
          );

          //bandManager.addPetitionWithParam(Tasks.SEND_MESSAGE_ALERT,bandManager.selectedBand,trad[title]);
          bandManager.selectedBand.sendMessageAlert(trad[title]);
          let date:Date=new Date(not.date);
          this.items.push({id:i,title:trad[title],content:content,icon:'alert',time:{subtitle:date.getDate()+" "+this.month[date.getMonth()],title:(date.getHours()<10?'0':'') + date.getHours()+":"+(date.getMinutes()<10?'0':'') + date.getMinutes()},expanded:true,alert:not.alertLevel})
          i++;
        },(e)=>{console.log(e); i++;}
      );
    }
  }
}
