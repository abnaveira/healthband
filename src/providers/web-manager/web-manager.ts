import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Events} from "ionic-angular";
import * as jwt_decode from "jwt-decode";
import {Storage} from "@ionic/storage";


@Injectable()
export class WebManagerProvider {
  public static BASIC_ULR:string ="http://ec2-18-218-112-146.us-east-2.compute.amazonaws.com:8080/hm-app-service/rest/";
  public static AUTH_TOKEN_KEY:string="AUTH_TOKEN_KEY";
  public static AUTH_URL:string=WebManagerProvider.BASIC_ULR+"authentication";
  public static USERS_URL:string=WebManagerProvider.BASIC_ULR+"users";
  public static USERS_REGISTER_URL:string=WebManagerProvider.BASIC_ULR+"users/register";
  public static USER_ME_URL:string=WebManagerProvider.BASIC_ULR+"users/me";
  public static MEASUREMENT_URL:string=WebManagerProvider.BASIC_ULR+"measurements";


  public token:string;
  public tokenInfo:any;
  public userInfo:any;
  public name:string='';
  public isAdmin=false;

  constructor(public http: HttpClient,public storage:Storage,public events: Events) {
  }

  public static changeEndpoint(newEndpoint){
  WebManagerProvider.BASIC_ULR =newEndpoint;
  WebManagerProvider.AUTH_TOKEN_KEY="AUTH_TOKEN_KEY";
  WebManagerProvider.AUTH_URL=WebManagerProvider.BASIC_ULR+"authentication";
  WebManagerProvider.USERS_URL=WebManagerProvider.BASIC_ULR+"users";
  WebManagerProvider.USERS_REGISTER_URL=WebManagerProvider.BASIC_ULR+"users/register";
  WebManagerProvider.USER_ME_URL=WebManagerProvider.BASIC_ULR+"users/me";
  WebManagerProvider.MEASUREMENT_URL=WebManagerProvider.BASIC_ULR+"measurements";
  }

/*  private managePreferences(){
    this.storage.get(PreferencesUtil.PREFERENCES_KEY+this.tokenInfo.sub).then(
      (prefs:any)=>{
        if(prefs==null){
          PreferencesUtil.setDefaultPreferences(this.tokenInfo.sub,this.storage);
        }
      },(e)=>{console.log(e);}
    );
  }*/

  public decodeTokenInfo(){
    try{
      this.tokenInfo= jwt_decode(this.token);
      this.isAdmin=this.tokenInfo.authorities.includes(1);
    }
    catch(Error){
      console.log(Error);
    }
  }

  public setStoreToken(t){
    this.storage.set(WebManagerProvider.AUTH_TOKEN_KEY,t).catch((e)=>{});
  }

  public logIn(todo){
    return new Promise((resolve,reject)=>{
      const time = setTimeout(()=>{
        let err={status:0};
        reject(err);
      },15000);

      const body = new HttpParams().set('username',todo.username).set('password',todo.password);
      this.http.post(WebManagerProvider.AUTH_URL,body.toString(),{headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),responseType:'text'}).subscribe(
        (value)=>{
          console.log(value);
          this.token=value;
          this.decodeTokenInfo();
/*
          this.managePreferences();
*/
          if(todo.rememberMe){
            this.setStoreToken(value);
          }
          clearTimeout(time);
          resolve();
        },
        (error)=>{
          clearTimeout(time);
          reject(error);
        }
      )
    })
  }

  public logOut(){
    this.setStoreToken(null);
  }

  public getUserOptions(){
      return new Promise((resolve,reject)=>{
        const time = setTimeout(()=>{
          let err={status:0};
          reject(err);
        },15000);
        this.http.get(WebManagerProvider.USERS_URL).subscribe(
          (value)=>{
            clearTimeout(time);
            resolve(value);
          },(e)=>{
            clearTimeout(time);
            reject(e);
          }
        )
      });
  }

  public register(data){
    return new Promise((resolve,reject)=>{
      const time = setTimeout(()=>{
        let err={status:0};
        reject(err);
      },15000);
      this.http.post(WebManagerProvider.USERS_REGISTER_URL,data,{headers: new HttpHeaders().set('Accept', 'application/json').set('Content-Type', 'application/json'),responseType:'text'}).subscribe(
        (value:string)=>{
          console.log(value);
          this.token=value;
          this.decodeTokenInfo();
/*
          this.managePreferences();
*/
          clearTimeout(time);
          resolve();
        },
        (error)=>{
          clearTimeout(time);
          reject(error)
      }
      )
    });
  }

  public getUserData(){
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + this.token);
    return new Promise((resolve,reject)=>{
      const time = setTimeout(()=>{
        let err={status:0};
        reject(err);
      },15000);

      this.http.get(WebManagerProvider.USER_ME_URL,{headers:headers}).subscribe(
        (data:any)=>{
          this.userInfo=data;
          this.name=data.name;
          clearTimeout(time);
          resolve(data);
        },(e)=>{
          clearTimeout(time);
          reject(e);
        }
      )
    })
  }

  public updateUserProfile(data){
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + this.token).set('Content-Type','application/json');
    return  new Promise((resolve,reject)=>{
      const time = setTimeout(()=>{
        let err={status:0};
        reject(err);
      },15000);
      this.http.post(WebManagerProvider.USER_ME_URL,data,{headers:headers}).subscribe(
        ()=>{
          clearTimeout(time);
          resolve();
          },
        (e)=>{
          clearTimeout(time);
          reject(e);
        }
      )
    })
  }

  public sendMeasurement(data){
    console.log(data);
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + this.token).set('Content-Type','application/json');
    return new Promise((resolve,reject)=>{
      const time = setTimeout(()=>{
        let err={status:0};
        reject(err);
      },15000);
      this.http.post(WebManagerProvider.MEASUREMENT_URL,data,{headers:headers}).subscribe(
        (notifications:any)=>{
          clearTimeout(time);
          resolve(notifications);
          },
        (e)=>{
          clearTimeout(time);
          reject(e);
        }
      )
    }) ;
  }

  public getMeasurements(){
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + this.token);
    return new Promise((resolve,reject)=>{
      const time = setTimeout(()=>{
        let err={status:0};
        reject(err);
      },15000);

      this.http.get(WebManagerProvider.MEASUREMENT_URL,{headers:headers}).subscribe(
        (value)=>{
          clearTimeout(time);
          resolve(value);
          },
        (e)=>{
          clearTimeout(time);
          console.log('publish web error event. ');
          reject(e);
        }
      )
    }) ;
  }

}
