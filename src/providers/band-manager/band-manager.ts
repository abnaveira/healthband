import { HttpClient } from '@angular/common/http';
import {Injectable, NgZone} from '@angular/core';
import {Storage} from "@ionic/storage";
import {BLE} from "@ionic-native/ble";
import {AlertController, Events, ModalController, ToastController} from "ionic-angular";
import {DataBaseProvider} from "../data-base/data-base";
import {BackgroundMode} from "@ionic-native/background-mode";
import { AndroidPermissions } from '@ionic-native/android-permissions';
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {WebManagerProvider} from "../web-manager/web-manager";
import {NotificationProvider} from "../notification/notification";
import {TranslateService} from "@ngx-translate/core";
import {Band, ConnectionStatus} from "./band";
import {BandFactory} from "./band-factory";
import {HeartRateSnapshotPage} from "../../pages/heart-rate-snapshot/heart-rate-snapshot";
import {ConfigurationProvider} from "../configuration/configuration";


export enum NotificationsID{
  NO_SELECTED_BAND,SELECTED_BAND_NOT_CONNECTED,BLUETOOTH,NOTIFICATION
}

@Injectable()
export class BandManagerProvider {
  SELECTED_DEVICE_KEY='SELECTED_DEVICE_KEY';
  DEVICES_KEY='DEVICES_KEY';

  public bands:Array<Band>;
  public selectedBand:Band;
  public backgroundJob:any;
  public dataFetch:boolean;

  noDevices:boolean=true;


  constructor(public http: HttpClient,public storage:Storage,public ble:BLE,
              public ngZone:NgZone,public events: Events,public db: DataBaseProvider,private backgroundMode: BackgroundMode,
              private locationAccuracy: LocationAccuracy,private androidPermissions:AndroidPermissions,
              private localNotifications: LocalNotifications,public webManager:WebManagerProvider,
              private notificationProvider:NotificationProvider,public translateService: TranslateService,
              public toastCtrl: ToastController,public modalCtrl:ModalController,public alertCtrl: AlertController,public configurationProvider:ConfigurationProvider) {
    this.bands=[];
    this.dataFetch=false;
    this.initializeNotificationsEvents();
  }

  /*
  *
  * Desc: Cambia la pulsera que se usará por defecto, si recibe null no habrá ninguna.
  *
  * */
  public setNewSelectedBand(device:Band){
    let doChange=()=>{
      /*Para asegurarnos de que solo haya una pulsera.*/
      for(let d of this.bands){
        d.isSelectedBand=false;
      }
      if(device!=null){
        /*Si la notificacón de no tener pulsera está activa, se elimina*/
        this.localNotifications.isPresent(NotificationsID.NO_SELECTED_BAND).then(
          (b)=>{
            if(b){this.localNotifications.clear(NotificationsID.NO_SELECTED_BAND).catch((e)=>{console.log(e);})}
          },(e)=>{console.log(e);}
        );

        /*Si el dispositivo que se intenta poner por defecto no está conectado se conecta*/
        if(device.connected==ConnectionStatus.DISCONNECTED){
          device.connect(false);
        }
        /*Se actualizan los valores de la pulsera por defecto.*/
        this.selectedBand=device;
        this.selectedBand.isSelectedBand=true;
        /*Se persiste el valor de la pulsera por defecto.*/
        console.log(device.id);
        this.storage.set(this.SELECTED_DEVICE_KEY+this.webManager.tokenInfo.sub,device.id).catch((e)=>{console.log(e)});
        this.translateService.get('DEFAULT_BAND_CHANGED').subscribe(
          (value:any)=> {
            this.toastCtrl.create({
              message:value,
              duration: 2000,
              position: 'bottom',
              cssClass:"toastStyle"
            }).present();
          },(e)=>{console.log(e);}
        );
      }else{
        this.selectedBand=null;
      }
    };


    if(this.selectedBand!=null && this.configurationProvider.loaded){
      let notShowAlert =this.configurationProvider.confValues.do_not_show_alert_band_change;
      if(!notShowAlert){
      this.translateService.get(["CHANGE_BAND_ALERT_TITLE","CHANGE_BAND_ALERT_BODY","OK","DO_NOT_SHOW_AGAIN"]).subscribe(
        (value)=> {
          let alert=this.alertCtrl.create({
            title: value.CHANGE_BAND_ALERT_TITLE,
            message: value.CHANGE_BAND_ALERT_BODY,
            buttons: [
              {
                text: value.OK,
                handler: (data:any) => {
                  if(data[0]=="value1"){
                    this.configurationProvider.confValues.do_not_show_alert_band_change=true;
                    this.configurationProvider.saveConfiguration()
                  }
                }
              }
            ]
          });
          alert.addInput({
            type: 'checkbox',
            label: value.DO_NOT_SHOW_AGAIN,
            value: 'value1',
            checked: false
          });
          alert.present();
        },(e)=>{console.log(e);});
      }


    }
    doChange();

  }

  /*
  *
  * Desc: Se elimina toda la info referente al usuario y se almacena la información necesaria (relativa a las pulseras). Se ejecuta al cerrar sesión
  * o al cerrar la aplicación.
  * */

  public logOut(){
    /*se persiste el valor de la pulsera por defecto*/
    if(this.selectedBand!=null){
      this.storage.set(this.SELECTED_DEVICE_KEY+this.webManager.tokenInfo.sub,this.selectedBand.id).catch((e)=>{console.log(e)})
    }else{
      this.storage.set(this.SELECTED_DEVICE_KEY+this.webManager.tokenInfo.sub,null).catch((e)=>{console.log(e)});
    }

    /*se persisten todas las pulseras que estén conectadas*/
    if(this.bands.length>0){
      this.storage.set(this.DEVICES_KEY+this.webManager.tokenInfo.sub,this.getSimplifiedDevices()).catch((e)=>console.log(e));
    }else{
      this.storage.set(this.DEVICES_KEY+this.webManager.tokenInfo.sub,null).catch((e)=>{console.log(e);});
    }

    /*se borran todos los valores de memoria*/
    this.bands=[];
    this.selectedBand=null;
    /*Si se está recuperando datos en segundo plano se para.*/
    if(this.dataFetch){
      this.stopBackgroundDataFetch();
      this.dataFetch=false;
    }
  }

  public requestLocation(){
    return new Promise((resolve,reject)=>{
      this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION,
        this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION]).then(
        ()=>{
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            if(canRequest) {
              // the accuracy option will be ignored by iOS
              this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_BALANCED_POWER_ACCURACY).then(
                () => {
                  console.log('Request location successful');
                  resolve();
                },
                error => {
                  console.log('Error requesting location permissions', error);
                  reject();
                }
              );
            }else{
              reject();
            }
          });
        },()=>{
          console.log("You must activate the location permission to do a scan.");
          reject();
        }
      )
    });
  }

  public requestBluetoothPermission(){
    return new Promise((resolve,reject)=>{
      this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.BLUETOOTH,
        this.androidPermissions.PERMISSION.BLUETOOTH_ADMIN]).then(
        ()=>{
          this.ble.enable().then(
            ()=>{
              this.localNotifications.isPresent(NotificationsID.BLUETOOTH).then(
                (b)=>{
                  if(b){this.localNotifications.clear(NotificationsID.BLUETOOTH).catch((e)=>{console.log(e);})}
                },(e)=>{console.log(e);}
              );
              resolve();
            },
            (e)=>{reject(e);}
          );
        },
        (e)=>{reject(e);}
      )
    });
  }

  /*Se conecta a todos los dispositivos que hayan sido persistidos*/
  public connectAllDevices(){
    console.log("connect all devices "+this.webManager.tokenInfo.sub);
    this.storage.get(this.SELECTED_DEVICE_KEY+this.webManager.tokenInfo.sub).then(
      (selectedBandId:string)=>{
        this.storage.get(this.DEVICES_KEY+this.webManager.tokenInfo.sub).then(
          (devices:any[])=>{
            console.log(devices);
            if(devices!=null){
              this.noDevices=devices.length<=0;
              for(let device of devices){
                let band = BandFactory.getBand(this,device.id,device.name);
                this.bands.push(band);
                band.connect(false);
                if(band.id==selectedBandId){
                  this.setNewSelectedBand(band);
                }
              }
            }else{
              this.noDevices=true;
              this.bands=[];
              this.selectedBand=null;
            }
          },()=>{}
        );
      },()=>{this.selectedBand=null;}
    );
  }


  /*Que acción realizar cuando se pulse sobre un push notification*/
  private initializeNotificationsEvents(){
    this.localNotifications.on("click").subscribe(
      (value:any)=>{
        if(value.id==NotificationsID.BLUETOOTH){
          this.requestBluetoothPermission().then(
            ()=>{
              this.startBackgroundDataFetch();
            }
          );
        }else if(value.id==NotificationsID.SELECTED_BAND_NOT_CONNECTED ){
          if(this.selectedBand!=null){
           this.selectedBand.connect(false);
            //setTimeout(()=>{if(!this.dataFetch){this.startBackgroundDataFetch();}},12000);
          }else{
            this.showNoSelectedBandNotification();
          }
        }else if(value.id==NotificationsID.NO_SELECTED_BAND){
          //Solo abre la aplicacion.
        }
      }
    );
  }

  public startBackgroundDataFetch(){
    console.log("start background fetch");
    return new Promise((resolve,reject)=>{
      if(this.selectedBand!=null){
        if(this.selectedBand.connected==ConnectionStatus.CONNECTED){
          this.dataFetch=true;
          this.backgroundMode.enable();

          if(this.configurationProvider.loaded){
            this.backgroundJob=setInterval(()=>{
                this.processData();}
              ,this.configurationProvider.confValues.dataFetchInterval*1000
            );
          }else{
            this.backgroundJob=setInterval(()=>{
                this.processData();}
              ,40*1000
            );
          }

          this.translateService.get('MONITORING_ENABLED').subscribe(
            (value:any)=> {
              this.toastCtrl.create({
                message:value,
                duration: 2000,
                position: 'bottom',
                cssClass:"toastStyle"
              }).present();
            },(e)=>{console.log(e);}
          );
          resolve();


        }else{
          this.showBandNotConnectedNotification()
          reject('The selected band is disconnected.');
        }
      }else{
        this.showNoSelectedBandNotification();
        reject('There is no selected band.');
      }
    })
  }

  public stopBackgroundDataFetch(){
    this.dataFetch=false;
    console.log("stop background fetch");
    this.translateService.get('MONITORING_DISABLED').subscribe(
      (value:any)=> {
        this.toastCtrl.create({
          message:value,
          duration: 2000,
          position: 'bottom',
          cssClass:"toastStyle"
        }).present();
      },(e)=>{console.log(e);}
    );
    this.backgroundMode.disable();
    clearTimeout(this.backgroundJob);
  }

  private processData(){
    this.getBackgroundData().then(
      (data:any)=>{
       this.sendDataToServer(data);
      },
      ()=>{
        console.log('An error ocurred when fetching data. Error code: ');
      }
    )
  }

  public sendDataToServer(data){
    let date = new Date(Date.now());
    //No enviamos mediciones con el hr a 0, representan errores (o que esta muerto).
    if(data.heartRate<=0){
      return;
    }
    let jsonData = {bandId:data.id,heartRate:data.heartRate,steps:data.steps,
      calories:data.calories,distance:data.distance, date:date};
    this.webManager.sendMeasurement(jsonData).then(
      (notifications:any)=>{

        this.notificationProvider.addNotifications(notifications,this);

        console.log("Data send");
      },()=>{
        console.log("Data send error");
        this.translateService.get("TOAST_SERVER_CONNECTION").subscribe(
          (value)=>{
            this.toastCtrl.create({
              message: value,
              duration: 1000,
              position: 'bottom',
              cssClass:"toastStyle"
            }).present();
          },(e)=>{console.log(e);}
        );
      }
    )
  }

  private getBackgroundData(){
    return new Promise((resolve,reject)=>{

      let onDataFetched=()=>{
        console.log('All data fetched, inserting..');
        //unsubscribe();
        this.events.unsubscribe(Band.DATA_FETCHED_EVENT);
        if(this.selectedBand!=null){
         let data = {id:this.selectedBand.id,steps:this.selectedBand.steps,calories:this.selectedBand.calories,
          distance:this.selectedBand.distance,heartRate:this.selectedBand.heartRateSnapshotValue};
          resolve(data);
        }else{
          this.showNoSelectedBandNotification();
          reject();
        }
      };

      this.ble.isEnabled().then(
        ()=>{
          if(this.selectedBand==null){
            this.showNoSelectedBandNotification();
            //unsubscribe();
            this.events.unsubscribe(Band.DATA_FETCHED_EVENT);
            this.stopBackgroundDataFetch();
            reject();
          }else if(this.selectedBand.connected!=ConnectionStatus.CONNECTED){
            this.showBandNotConnectedNotification();
            this.clearPetitions(this.selectedBand);
            //unsubscribe();
            this.events.unsubscribe(Band.DATA_FETCHED_EVENT);
            this.stopBackgroundDataFetch();
            reject();
          }else{
            this.events.subscribe(Band.DATA_FETCHED_EVENT,()=>{onDataFetched()});
            this.selectedBand.collectData();
          }
        },()=>{
          this.events.unsubscribe(Band.DATA_FETCHED_EVENT);
          this.stopBackgroundDataFetch();
          this.showBluetoothNotification();
          reject();
        }
      );
    });
  }



  private showBandNotConnectedNotification(){
    this.translateService.get('NOT_SELECTED_BAND_NOT_CONNECTED').subscribe(
      (value:any)=>{
        this.localNotifications.schedule({
          id: NotificationsID.SELECTED_BAND_NOT_CONNECTED,
          text: value,
          //sticky:true
        });

        this.events.subscribe(Band.SELECTED_BAND_CONNECTED_EVENT,()=>{
          this.localNotifications.isPresent(NotificationsID.SELECTED_BAND_NOT_CONNECTED).then(
            (b)=>{
              if(b){this.localNotifications.clear(NotificationsID.SELECTED_BAND_NOT_CONNECTED).catch((e)=>{console.log(e);})}
            },(e)=>{console.log(e);});
        });
    },(e)=>{console.log(e);}
    );

  }

  private showBluetoothNotification(){
    this.translateService.get('NOT_BLUETOOTH').subscribe(
      (value:any)=>{
        this.localNotifications.schedule({
          id: NotificationsID.BLUETOOTH,
          text: value,
          //sticky:true
        });
      },(e)=>{console.log(e);}
    );



  }

  private showNoSelectedBandNotification(){
    this.translateService.get('NOT_NO_SELECTED_BAND').subscribe(
      (value:any)=>{
        this.localNotifications.schedule({
          id: NotificationsID.NO_SELECTED_BAND,
          text: value,
          //sticky:true
        });
      },(e)=>{console.log(e);}
    );


  }

  public addDevice(band:Band){
    console.log("add device: "+this.webManager.tokenInfo.sub);
    if(this.bands.length<=0){
      this.setNewSelectedBand(band);
    }else{
      for(let device of this.bands){
        if(device.id==band.id){
          this.ngZone.run(()=>{
            device.connected=band.connected;
          });
          this.storage.set(this.DEVICES_KEY+this.webManager.tokenInfo.sub,this.getSimplifiedDevices());
          return;
        }
      }
    }

    this.ngZone.run(()=>{
      this.bands.push(band);
    });
    if(this.bands!=null){
      this.noDevices=this.bands.length<=0;
    }else{
      this.noDevices=true;
    }
    this.storage.set(this.DEVICES_KEY+this.webManager.tokenInfo.sub,this.getSimplifiedDevices());
  }

  public doDeleteDevice(band:Band){
    //this.disconnect(band);
    band.disconnect();
    const index = this.bands.indexOf(band, 0);
    if (index > -1) {
      this.bands.splice(index, 1);
    }
    if(band.isSelectedBand){
      this.setNewSelectedBand(null);
      this.storage.set(this.SELECTED_DEVICE_KEY+this.webManager.tokenInfo.sub,null);

    }
    this.storage.set(this.DEVICES_KEY+this.webManager.tokenInfo.sub,this.getSimplifiedDevices());
    if(this.bands!=null){
      this.noDevices=this.bands.length<=0;
    }else {
      this.noDevices=true;
    }
  }

  public containsDevice(id:string){
    for (let band of this.bands){
      if(band.id==id)
        return true;
    }
    return false;
  }

  private clearPetitions(dev:Band){
    dev.petitionQueue=[];
    dev.queueBusy=false;
  }


  private getSimplifiedDevices():{id:string,name:string}[]{
    let simp:{id:string,name:string}[]=[];
    for (let band of this.bands){
      simp.push({id:band.id,name:band.name});
    }
    return simp;
  }

  updateDefaultDataAllDevices() {
    for(let band of this.bands){
      band.updateDefaultData();
    }
  }

  getDefaultBandBatteryPercentage():string{
    if(this.selectedBand!=null){
      return this.selectedBand.battery;
    }
    return '0';
  }

  getDefaultBandSteps(){
    if(this.selectedBand!=null){
      return this.selectedBand.steps;
    }
    return '0';
  }

  getDefaultBandDistance(){
    if(this.selectedBand!=null){
      return this.selectedBand.distance;
    }
    return '0';
  }

  getDefaultBandCalories(){
    if(this.selectedBand!=null){
      return this.selectedBand.calories;
    }
    return '0';
  }

  getDefaultBandName(){
    if(this.selectedBand!=null){
      return this.selectedBand.name;
    }
    return 'None';
  }


  public connectDisconnect(device:Band) {
    this.requestBluetoothPermission().then(
      ()=>{
        if(device.connected==ConnectionStatus.DISCONNECTED){
          //this.bandManager.addPetition(Tasks.CONNECT,device);
          device.connect(false);
        }else if(device.connected==ConnectionStatus.CONNECTED){
          //this.bandManager.disconnect(device);
          device.disconnect();
        }
      },()=>{
        this.showBluetoothConnectionToast();
      }
    )
  }

  private showBluetoothConnectionToast(){
    this.translateService.get("TOAST_ENABLE_BLUETOOTH").subscribe(
      (value)=>{
        this.toastCtrl.create({
          message: value,
          duration: 1000,
          position: 'bottom',
          cssClass:"toastStyle"
        }).present();
      },(e)=>{console.log(e);}
    )
  }

  public heartRateSnapshot(device: Band) {
    if(device.connected==ConnectionStatus.CONNECTED){
      device.heartRateSnapshotMeasured=false;
      this.requestBluetoothPermission().then(
        ()=>{
          device.heartRateSnapshot();
          let modal = this.modalCtrl.create(HeartRateSnapshotPage,{dev:device});
          modal.present();
          modal.onDidDismiss(data => {console.log(data)});
        },
        ()=>{
          this.showBluetoothConnectionToast();
        }
      )
    }
  }
  public deleteDevice(device: Band) {
    this.translateService.get(["DELETE_BAND","ARE_YOU_SURE","YES","NO"]).subscribe(
      (value)=>{
        const confirm = this.
        alertCtrl.create({
          title: value.DELETE_BAND,
          message: value.ARE_YOU_SURE,
          buttons: [
            {
              text: value.NO,
              handler: () => {}
            },
            {
              text: value.YES,
              handler: () => {
                this.doDeleteDevice(device);
              }
            }
          ]
        });
        confirm.present();
      }
    )
  }

  public findDevice(device: Band) {
    if(device.connected==ConnectionStatus.CONNECTED){
      this.requestBluetoothPermission().then(
        ()=>{
          device.sendSimpleAlert();
        },()=>{
          this.showBluetoothConnectionToast();
        }
      );
    }
  }

  public changeSelectedBand(device: Band) {
    if(!device.isSelectedBand){
      this.setNewSelectedBand(device);
    }else{
      this.setNewSelectedBand(null);
    }
  }


}
