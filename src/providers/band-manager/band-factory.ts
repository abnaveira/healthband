import {MiBand2} from "./mi-band-2";
import {BandManagerProvider} from "./band-manager";
import {Band} from "./band";

export enum SupportedBands {
  MI_BAND_2='MI Band 2'
}

export class BandFactory {
  public static getBand(bandManager:BandManagerProvider,id:string,name:string):Band{
    if(name==SupportedBands.MI_BAND_2){
      return new MiBand2(bandManager,id,name);
    }
    return null;
  }

  public static bandSupported(bandName:string):boolean{
    if(bandName==SupportedBands.MI_BAND_2){
      return true;
    }
    return false;
  }
}
