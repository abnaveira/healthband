import {BandManagerProvider} from "./band-manager";
import * as CryptoJS from 'crypto-js';
import {Buffer} from 'buffer';
import {Band, ConnectionStatus, Tasks} from "./band";
import {TextDecoder,TextEncoder} from 'text-encoding';



export class MiBand2 extends Band{
  static AUTH_CHARACTERISTIC="00000009-0000-3512-2118-0009af100700";
  static AUTH_SERVICE="0000fee1-0000-1000-8000-00805f9b34fb";
  static HARDWARE_SERVICE="0000fee0-0000-1000-8000-00805f9b34fb";
  static SETTINGS_CHARACTERISTIC="00000008-0000-3512-2118-0009af100700";
  static BATTERY_CHARACTERISTIC="00000006-0000-3512-2118-0009af100700";
  static ACTIVITY_CHARACTERISTIC="00000007-0000-3512-2118-0009af100700";
  static HEART_RATE_SERVICE="180D";
  static HEART_MONITOR_CONTROL_CHARACTERISTIC ="2A39";
  static HEART_RATE_MEASUREMENT_CHARACTERISTIC="2A37";
  static DEVICE_INFORMATION_SERVICE="180a";
  static HARDWARE_REVISION_CHARACTERISTIC="2a27";
  static SOFTWARE_REVISION_CHARACTERISTIC="2a28";
  static CUSTOM_ALERT_SERVICE="1811";
  static CUSTOM_ALERT_CHARACTERISTIC="2a46";
  static INMEDIATE_ALERT_SERVICE="1802";
  static INMEDIATE_ALERT_CHARACTERISTIC="2a06";

  private TAG:string = "MI Band 2";

  constructor(public bandManager:BandManagerProvider,id:string,name:string) {
    super(id,name,bandManager);
  }


  /*****************************************************/
  public connect(needsAuth:boolean){
    this.addPetition({task:Tasks.CONNECT,params:{needsAuth:needsAuth}},true);
  }

  public doConnect(needsAuth:boolean){
    return this.connectAux(needsAuth);
  }

  private connectAux(needsAuth){
    return new Promise((resolve,reject)=>{
      this.bandManager.ble.connect(this.id).subscribe(
        (dev)=>{
          this.activateAuthNotification().then(
            ()=>{
              resolve();
            },
            ()=>{
              reject();
            }
          );

          let f1 = ()=>{
            this.sendAuthRequest().catch((e)=>{reject(e);});
          }
          let f2 = ()=>{
            this.sendReconnect().catch((e)=>{reject(e);});
          };

          if(needsAuth) {
            //Empieza la autenticación después de 2 segundos;
            setTimeout(f1,2000);
          }else{
            setTimeout(f2,2000);
          }
        },

        (error)=>{
          this.connected=ConnectionStatus.DISCONNECTED;
          console.log(error);
          reject(error);
        })
    });
  }

  private sendAuthRequest() {
    let val :Uint8Array= new Uint8Array([0x01,0x80,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x40,0x41,0x42,0x43,0x44,0x45]);
    return this.bandManager.ble.writeWithoutResponse(this.id,MiBand2.AUTH_SERVICE,MiBand2.AUTH_CHARACTERISTIC,val.buffer);
  }

  private sendReconnect() {
    let test: Uint8Array = new Uint8Array([0x02, 0x80]);
    return this.bandManager.ble.writeWithoutResponse(this.id,MiBand2.AUTH_SERVICE,MiBand2.AUTH_CHARACTERISTIC, test.buffer);
  }

  private activateAuthNotification() {
    return new Promise((resolve,reject)=>{
      console.log(this.TAG+'authentication process started.');
      this.bandManager.ble.startNotification(this.id,MiBand2.AUTH_SERVICE,MiBand2.AUTH_CHARACTERISTIC).subscribe(
        (buffer)=>{
          console.log(this.TAG+'new notification from Auth characteristic');
          let val:Uint8Array=new Uint8Array(buffer);
          /*en hex sería: [0x10,0x01,0x01]*/
          if(val[0]===16 && val[1]===1 && val[2]===1){
            let val:Uint8Array=new Uint8Array([0x02,0x80]);
            this.bandManager.ble.writeWithoutResponse(this.id,MiBand2.AUTH_SERVICE,MiBand2.AUTH_CHARACTERISTIC,val.buffer);

          }else if(val[0]===16 && val[1]===2 && val[2]===1){

            let mValue:Uint8Array=val.subarray(3,19);
            let secretKey: Uint8Array=new Uint8Array([0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45]);
            let message = CryptoJS.enc.Hex.parse(Buffer.from(mValue).toString('hex'));
            let key = CryptoJS.enc.Hex.parse(Buffer.from(secretKey).toString('hex'));
            let encrypt = CryptoJS.AES.encrypt(message,key,{mode: CryptoJS.mode.ECB,padding:CryptoJS.pad.NoPadding});
            let val1 ="0380"+CryptoJS.enc.Hex.stringify(encrypt.ciphertext);
            let typedArray = new Uint8Array(val1.match(/[\da-f]{2}/gi).map(function (h) {
              return parseInt(h, 16)
            }));
            this.bandManager.ble.writeWithoutResponse(this.id,MiBand2.AUTH_SERVICE,MiBand2.AUTH_CHARACTERISTIC,typedArray.buffer).catch((e)=>{reject(e)})

          }else if(val[0]===16 && val[1]===3 && val[2]===1){
              console.log(this.TAG+'Mi band 2 authenticated.');
              this.connected=ConnectionStatus.CONNECTED;

            this.bandManager.addDevice(this);
            this.bandManager.ble.stopNotification(this.id,MiBand2.AUTH_SERVICE,MiBand2.AUTH_CHARACTERISTIC).catch((e)=>{
              this.connected=ConnectionStatus.DISCONNECTED;
              reject(e);
            });
            resolve();
          }
        },
        (error)=> {
          reject('Auth notification error: '+error);
        });
    })
  }


/****************************************************/
  heartRateSnapshot(){
    this.addPetition({task:Tasks.GET_HEART_RATE_SNAPSHOT,params:null},false)
  }

  doHeartRateSnapshot(): any {
    return new Promise((resolve,reject)=>{
      this.activateHeartRateSnapshotNotification().then(
        ()=>{
          this.heartRateSnapshotMeasured=true;
          resolve();
        },(error)=>{
          this.heartRateSnapshotMeasured=true;
          reject(error);
        }
      );
      setTimeout(this.writeHeartRateSnapshotPetition(),3000);
    });
  }

  private writeHeartRateSnapshotPetition(){
    let val = new Uint8Array([0x15,0x02,0x01]);
    this.bandManager.ble.write(this.id,MiBand2.HEART_RATE_SERVICE,MiBand2.HEART_MONITOR_CONTROL_CHARACTERISTIC,val.buffer);
  }

  private activateHeartRateSnapshotNotification(){
    return new Promise((resolve,reject)=>{
      this.bandManager.ble.startNotification(this.id,MiBand2.HEART_RATE_SERVICE,MiBand2.HEART_RATE_MEASUREMENT_CHARACTERISTIC).subscribe(
        (buffer)=>{
          let val=new Uint8Array(buffer);
          let hr = val[1]&0xff;
          this.heartRateSnapshotValue=hr.toString();
          console.log(hr);
          resolve();
        },
        (error)=>{
          reject(error);
        }
      )
    })
  }
/*************************************************************************/
  sendMessageAlert(text:string){
    this.addPetition({task:Tasks.SEND_MESSAGE_ALERT,params:{text:text}},false);
  }

  doSendMessageAlert(text: string): any {
    let message=new Uint8Array(new TextEncoder('utf-8').encode(text));
    let petition = new Uint8Array([5,4]);
    let value= new Uint8Array(petition.length+message.length);
    value.set(petition);
    value.set(message,petition.length);
    console.log(value);
    this.bandManager.ble.write(this.id,MiBand2.CUSTOM_ALERT_SERVICE,MiBand2.CUSTOM_ALERT_CHARACTERISTIC,value.buffer);
    return new Promise((resolve,reject)=>{
      resolve();
    })
  }

  sendSimpleAlert(){
    this.addPetition({task:Tasks.SEND_INMEDIATE_ALERT,params:null},false);
  }

  doSendSimpleAlert(): any {
    let value = new Uint8Array([1]);
    this.bandManager.ble.writeWithoutResponse(this.id,MiBand2.INMEDIATE_ALERT_SERVICE,MiBand2.INMEDIATE_ALERT_CHARACTERISTIC,value.buffer);
    return new Promise((resolve,reject)=>{
      resolve();
    })
  }
/*******************************************************************************/
  updateActivity(){
    this.addPetition({task:Tasks.GET_ACTIVITY,params:{}},false);
  }

  doUpdateActivity(): any {
    console.log('getActivity');
    return new Promise((resolve,reject)=>{
      this.bandManager.ble.read(this.id,MiBand2.HARDWARE_SERVICE,MiBand2.ACTIVITY_CHARACTERISTIC).then(
        (buffer)=>{
          let data=new Uint8Array(buffer);
          let steps= ((((data[1] & 255) | ((data[2] & 255) << 8))) );
          this.steps=steps.toString();
          let dist=((((data[5] & 255) | ((data[6] & 255) << 8)) | (data[7] & 16711680)) | ((data[8] & 255) << 24));
          this.distance=dist.toString();
          let calories = ((((data[9] & 255) | ((data[10] & 255) << 8)) | (data[11] & 16711680)) | ((data[12] & 255) << 24));
          this.calories=calories.toString();
          resolve();
        },
        (error)=>{
          reject(error);
        }
      );
    });
  }

  /*****************************************************************************/
  updateBattery(){
    this.addPetition({task:Tasks.READ_BATTERY,params:null},false);
  }

  doUpdateBattery(): any {
    console.log('updateBattery');
    return new Promise((resolve,reject)=>{
      this.bandManager.ble.read(this.id,MiBand2.HARDWARE_SERVICE,MiBand2.BATTERY_CHARACTERISTIC).then(
        (buffer)=>{
          let val:Uint8Array=new Uint8Array(buffer);
            this.battery=val[1].toString();
          resolve();
        },
        (error)=>{
          reject(error);
        });
    })
  }

  /**************************************************************/

  updateDeviceInfo(){
    this.addPetition({task:Tasks.GET_DEVICE_INFO,params:null},false);
  }

  doUpdateDeviceInfo(): any {
    return new Promise((resolve,reject)=>{
      this.bandManager.ble.read(this.id,MiBand2.DEVICE_INFORMATION_SERVICE,MiBand2.HARDWARE_REVISION_CHARACTERISTIC).then(
        (buffer)=>{
          let val = new Uint8Array(buffer);
          let decoder = new TextDecoder('utf-8');
          this.hardwareRevision=decoder.decode(val);
          this.bandManager.ble.read(this.id,MiBand2.DEVICE_INFORMATION_SERVICE,MiBand2.SOFTWARE_REVISION_CHARACTERISTIC).then(
            (buffer)=>{
              let val= new Uint8Array(buffer);
              let decoder = new TextDecoder('utf-8');
              this.softwareRevision=decoder.decode(val);
              resolve();
            },
            (error)=>{
              reject(error);
            }
          )
        },
        (error)=>{
          reject(error);
        }
      )
    })
  }
/**********************************************************************/
  disconnect(): any {
    this.addPetition({task:Tasks.DISCONNECT,params:null},false);
  }

  private doDisconnect(){
    return new Promise((resolve ,reject)=>{
      this.connected=ConnectionStatus.DISCONNECTING;
      console.log("Disconnecting device : "+this.id);
      this.bandManager.ble.disconnect(this.id).then(
        ()=>{
          this.connected=ConnectionStatus.DISCONNECTED;
          resolve();
        },
        (error)=>{
          console.log(error);
          reject();
        }
      );
    });

  }

  /*******************************************************************/
  collectData(): any {
    if(this.isSelectedBand){
      this.updateActivity();
      this.heartRateSnapshot();
      this.addPetition({task:Tasks.DATA_RECOLECT_COMPLETED,params:null},false)
    }
  }

  /********************************************************************/
  public addPetition(data:{task:Tasks,params:any},addAsFirst:boolean){
    /*Se puede forzar a que una tarea se ponga de primera en la cola*/
    if(addAsFirst){
      this.petitionQueue.unshift(data)
    }else{
      this.petitionQueue.push(data);
    }

    this.onTaskAddedToQueue(data.task);

    if(!this.queueBusy){
      this.queueBusy=true;
      this.executeQueueTask();
    }
  }

  private executeQueueTask(){

    if(this.petitionQueue.length<=0){
      console.log('No more tasks left');
      this.queueBusy=false;
      return;
    }
    let task= this.petitionQueue[0].task;
    /*Se establece un timeout de 10 segundos para realizar una tarea.*/
    let taskTimer = setTimeout(()=>{this.taskTimeout(task)},10000);

    this.executeTask(this.petitionQueue[0]).then(
      ()=>{
        /*Si se completa la tarea se elimina el timeout*/
        clearTimeout(taskTimer);
        console.log('Tarea completada. ' + Tasks[task] + ' in device: '+this.id);

        this.onTaskCompleted(task);


        this.petitionQueue.shift();
        this.executeQueueTask();
      },
      ()=>{
        clearTimeout(taskTimer);
        this.handleTaskError(task);
      }
    )
  }

  private executeTask(data:{task:Tasks,params:any}){
    console.log('Execute task: '+ Tasks[data.task] +'in device: '+this.id);

    //Cualquier peticion que no sea connectarse a la pulsera requiere que la pulsera esté conectada.
    if(data.task!=Tasks.CONNECT && this.connected!=ConnectionStatus.CONNECTED){
      return Promise.reject('Band is not connected.')
    }
    switch (data.task){
      case Tasks.READ_BATTERY:{
        return this.doUpdateBattery();
      }
      case Tasks.CONNECT:{
        return this.doConnect(data.params.needsAuth);
      }
      case Tasks.GET_ACTIVITY:{
        return this.doUpdateActivity();
      }
      case Tasks.GET_HEART_RATE_SNAPSHOT:{
        return this.doHeartRateSnapshot();
      }
      case Tasks.GET_DEVICE_INFO:{
        return this.doUpdateDeviceInfo();
      }
      case Tasks.SEND_INMEDIATE_ALERT:{
        return this.doSendSimpleAlert();
      }
      case Tasks.SEND_MESSAGE_ALERT:{
        return this.doSendMessageAlert(data.params.text);
      }
      case Tasks.DISCONNECT:{
        return this.doDisconnect();
      }
      case Tasks.DATA_RECOLECT_COMPLETED:{
        this.bandManager.events.publish(Band.DATA_FETCHED_EVENT);
        return Promise.resolve();
      }

      default:{
        return new Promise((resolve,reject)=>{
          reject();
        })
      }
    }
  }

  /*esta funcion establece la acción a realizar si una tarea tarda más tiempo del debido en ejecutarse*/
  private taskTimeout(task:Tasks){
    this.handleTaskError(task);
  }

  /*Acción a realizar tras completar una tarea*/
  private onTaskCompleted(task:Tasks){
    if(task===Tasks.CONNECT){
      setTimeout(()=>{this.updateDefaultData();},2000);
      if(this.isSelectedBand){

      }
    }
  }

  public updateDefaultData(){
    if(this.connected==ConnectionStatus.CONNECTED){
      this.updateBattery();
      this.updateActivity();
      this.updateDeviceInfo();
    }
  }

  /*acción a realizar cuando se añade una tarea a la cola*/
  private onTaskAddedToQueue(task:Tasks){
    if(task==Tasks.CONNECT){
      this.connected=ConnectionStatus.CONNECTING;
    }
  }

  /*Acción a realizar si ocurre algún problema con una tarea*/
  private handleTaskError(task:Tasks){
    console.log('Ha habido un problema en la cola de tareas con la tarea: '+Tasks[task]);
    /*Si la tarea es conectarse se vacía la cola de las demás tareas ya que no tiene sentido hacer más peticiones si no se conecta*/
    if(task===Tasks.CONNECT){
      this.petitionQueue=[];
      this.queueBusy=false;
      this.connected=ConnectionStatus.DISCONNECTED;
    }else {
      this.petitionQueue.shift();
      this.executeQueueTask();
    }
  }

}
