import {BandManagerProvider} from "./band-manager";

export enum ConnectionStatus {
  CONNECTED,CONNECTING,DISCONNECTED,DISCONNECTING
}
export enum Tasks {
  READ_BATTERY,
  CONNECT,
  GET_ACTIVITY,
  GET_HEART_RATE_SNAPSHOT,
  GET_DEVICE_INFO,
  SEND_INMEDIATE_ALERT,
  SEND_MESSAGE_ALERT,
  DATA_RECOLECT_COMPLETED,
  DISCONNECT
}
export abstract class Band {
  public static DATA_FETCHED_EVENT="DATA_FETCHED_EVENT";
  public static SELECTED_BAND_CONNECTED_EVENT="SELECTED_BAND_CONNECTED_EVENT";
  public static BAND_CONNECTED_EVENT="BAND_CONNECTED_EVENT";


  id: string;
  name: string;
  battery: string;
  steps: string;
  distance: string;
  calories: string;
  hardwareRevision: string;
  softwareRevision: string;
  private _connected: ConnectionStatus;
  connectedString: string;
  connectedBool: boolean;
  isSelectedBand: boolean;
  petitionQueue: {task:Tasks,params:any}[];
  queueBusy:boolean;
  heartRateSnapshotValue:string;
  heartRateSnapshotMeasured:boolean;
  rssi:string;


  constructor(id: string, name: string,public bandManager:BandManagerProvider) {
    this.id = id;
    this.name = name;
    this.battery='0';
    this.steps='0';
    this.distance='0';
    this.calories='0';
    this.hardwareRevision='';
    this.softwareRevision='';
    this.connected=ConnectionStatus.DISCONNECTED;
    this.isSelectedBand=false;
    this.petitionQueue=[];
    this.queueBusy=false;
    this.heartRateSnapshotValue='0';
    this.heartRateSnapshotMeasured=false;
    this.rssi='';
  }

  get connected(): ConnectionStatus {
    return this._connected;
  }

  set connected(value: ConnectionStatus) {
    this._connected = value;
    this.updateConnectedString()
  }

  abstract connect(needsAuth:boolean): any;

  abstract disconnect():any;

  abstract updateBattery(): any;

  abstract updateActivity(): any;

  abstract heartRateSnapshot(): any;

  abstract updateDeviceInfo(): any;

  abstract sendMessageAlert(text: string): any;

  abstract sendSimpleAlert():any;

  abstract collectData():any;

  abstract updateDefaultData():any;

  private updateConnectedString(){
    if(this.connected==ConnectionStatus.CONNECTED){
      this.connectedString='DISCONNECT';
      this.connectedBool=true;
      /*se envía un evento de que la pulsera se ha conectado*/
      this.bandManager.events.publish(Band.BAND_CONNECTED_EVENT+this.id);
    }else if(this.connected==ConnectionStatus.DISCONNECTED){
      this.connectedString='CONNECT';
      this.connectedBool=false;
    }else if(this.connected==ConnectionStatus.CONNECTING){
      this.connectedString='CONNECTING';
      this.connectedBool=false;
    }else if(this.connected==ConnectionStatus.DISCONNECTING){
      this.connectedString='DISCONNECTING';
      this.connectedBool=false;
    }
  }

}




