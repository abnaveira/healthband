import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Storage} from "@ionic/storage";
import {WebManagerProvider} from "../web-manager/web-manager";
import {TranslateService} from "@ngx-translate/core";
import {Events} from "ionic-angular";

/*
  Generated class for the ConfigurationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConfigurationProvider {
  public static PREFERENCES_KEY:string='PREFERENCES_KEY';
  public static PREFERENCES_CHANGED_EVENT:string='PREFERENCES_CHANGED_EVENT';
  public static PREFERENCES_LOADED_EVENT:string='PREFERENCES_CHANGED_EVENT';

  public confValues:any;
  public confOptions:any;
  public loaded:boolean=false;

  public idioms=[{
    value: 'es',
    label: 'Español'
  },
    {
      value: 'en',
      label: 'English'
    }];
  constructor(public http: HttpClient,public storage:Storage,public webManager:WebManagerProvider,private translateService: TranslateService,public events:Events) {
    this.storage.get(ConfigurationProvider.PREFERENCES_KEY+this.webManager.tokenInfo.sub).then(
      (prefs:any)=>{
        if(prefs==null){
          this.http.get('assets/configuration/defaultConfiguration.json').subscribe(
            (defaultPrefs:any)=>{
              storage.set(ConfigurationProvider.PREFERENCES_KEY+this.webManager.tokenInfo.sub,defaultPrefs);
              this.confValues=defaultPrefs;
            },(e)=>{console.log(e);this.loaded=false;}
          );
          }else{
          console.log(prefs);
          this.confValues=prefs;
        }
        this.http.get('assets/configuration/configurationPossibilities.json').subscribe(
          (options)=>{
            this.confOptions=options;
            this.loaded=true;
            this.events.publish(ConfigurationProvider.PREFERENCES_LOADED_EVENT);

          },(e)=>{console.log(e); this.loaded=false;}
        );
      });



  }

  public saveConfiguration(){
    this.storage.set(ConfigurationProvider.PREFERENCES_KEY+this.webManager.tokenInfo.sub,this.confValues);
    this.events.publish(ConfigurationProvider.PREFERENCES_CHANGED_EVENT);
  }

  public setLanguage(lang){
    console.log(lang);
    this.translateService.use(lang);
  }

}
